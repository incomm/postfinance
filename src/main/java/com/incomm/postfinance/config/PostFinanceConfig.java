package com.incomm.postfinance.config;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConversionException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.logging.LogConstants;

@Service(value="config")
public class PostFinanceConfig implements LogConstants{

	private final String encryptionPassword = "supercloud";
	protected Logger log = Logger.getLogger(PostFinanceConfig.class);

	Configuration props = null;
	@Autowired StandardPBEStringEncryptor configurationEncryptor;

	public PostFinanceConfig() throws ConfigurationException {

		load(getPropertiesFile());
	}

	@PostConstruct
	public void init() {
		try {
			log.info("Decrypting properties.");
			// do NOT log decrypted values
			configurationEncryptor.setPassword(hash(encryptionPassword));
			decryptProperty(PropertyConstants.OAUTH2_CLIENT_ID);
			decryptProperty(PropertyConstants.OAUTH2_CLIENT_SECRET);
			
		} catch (Exception e) {
			// Log error for NOC
			log.fatal(PP_CODE + "=" + PROPERTIES_DECRYPTION_CODE);
			log.info("Failed to decrypt properties.");
			
			// In production, throw the exception to prevent successful start-up
			if (getDeployment().equals(PropertyConstants.PRODUCTION)) {
				throw e;
			}
		}
	}
	
	public String getDeployment() {

		String deploy = System.getProperty(PropertyConstants.DEPLOYMENT_TYPE_ENV_VAR);
		if (deploy == null) {
			deploy = PropertyConstants.DEFAULT_DEPLOYMENT;
		}
		return deploy;
	}
	
	private String getPropertiesFile() {
		
		return "postfinance_" + getDeployment() + ".properties";
	}

	private void load(String propsFile) throws ConfigurationException {

		propsFile =  propsFile == null ? getPropertiesFile() : propsFile; 
		log.info("Loading properties from " + propsFile);
		props = new PropertiesConfiguration(propsFile);
		logProperties();
	}

	private void decryptProperty(String key) {
		// do NOT log decrypted values
		String encrypted = getProperty(key);
		String decrypted = configurationEncryptor.decrypt(encrypted);
		setProperty(key, decrypted);
	}

	private void logProperties() {
		// Log properties
		StringBuilder sb = new StringBuilder();
		Iterator<String> iter = props.getKeys();
		while (iter.hasNext()) {
			String key = iter.next();
			sb.append("\n" + key + ": " + props.getString(key));
		}
		log.info("Post Finance properties:" + sb.toString());
	}

	public void setProperty(String key, String value) {

		props.setProperty(key, value);
	}

	public String[] getPropertyAsStringArray(String key) {
		String[] value = props.getStringArray(key);
		if (value == null) {
			log.info("Property with key [" + key + "] was not found, returning null.");
		}
		return value;
	}

	/**
	 * Returns the value of a property with the given key.
	 * @see {@link #getProperty(String, String)}
	 * 
	 * @param key
	 * @return Value of property or null if no mapping exists.
	 */
	public String getProperty(String key) {

		return getProperty(key, null);
	}

	/**
	 * Returns the value of a property with the given key.
	 * 
	 * @param key
	 * @param defaultValue
	 * @return Value of property or the default value if no mapping exists.
	 */
	public String getProperty(String key, String defaultValue) {

		String value = props.getString(key, defaultValue);
		if (value == null) {
			log.debug("Property with key [" + key + "] was not found, returning null.");
		}
		return value;
	}

	/**
	 * Returns the value of a property with the given key as a long.
	 * 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public long getPropertyAsLong(String key, long defaultValue) {

		try {
			return props.getLong(key, defaultValue);
		} catch (ConversionException e) {
			log.debug("Failed to parse property with key [" + key + "] as a long value.");
			return defaultValue;
		}	
	}
	
	/**
	 * Returns the value of a property with the given key as an int.
	 * 
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	public int getPropertyAsInt(String key, int defaultValue) {
		
		try {
			return props.getInt(key, defaultValue);
		} catch (ConversionException e) {
			log.debug("Failed to parse property with key [" + key + "] as an int value.");
			return defaultValue;
		}
	}
	
	/**
	 * Performs MD5 hash of input string.
	 * 
	 * @param toHash
	 * @return
	 */
	public String hash(String toHash) {
		
        String hashed = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] bytes = md.digest(toHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            hashed = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hashed;
    }
	
}
