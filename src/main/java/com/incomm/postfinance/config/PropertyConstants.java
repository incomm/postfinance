package com.incomm.postfinance.config;

public interface PropertyConstants {
	
	// Environment Variables
	public static final String DEPLOYMENT_TYPE_ENV_VAR		= "env";
	
	// Environments
	public static final String PRODUCTION					= "PROD";
	public static final String TESTING						= "UAT";
	public static final String DEVELOPMENT				 	= "DEV";
	public static final String DEFAULT_DEPLOYMENT			= TESTING;
	
	// OAuth2 Message Properties
	public static final String OAUTH2_CLIENT_ID 			= "oauth2.client.id";
	public static final String OAUTH2_CLIENT_SECRET 		= "oauth2.client.secret";
	public static final String OAUTH2_SCOPE 				= "oauth2.scope";
	
	// URLs
	public static final String OAUTH2_URL 					= "rtg.oauth2.url";
	public static final String RTG_PURCHASE_URL				= "rtg.digital.purchase.url";
	public static final String RTG_CANCEL_URL				= "rtg.digital.cancel.url";
	public static final String RTG_GETSTATE_URL				= "rtg.digital.getstate.url";
	
	// Timeout Properties
	public static final String RTG_RECEIVE_TIMEOUT			= "rtg.digital.timeout.receive";
	public static final String RTG_CONNECT_TIMEOUT			= "rtg.digital.timeout.connect";
	public static final String OAUTH2_RECEIVE_TIMEOUT		= "oauth2.timeout.receive";
	public static final String OAUTH2_CONNECT_TIMEOUT		= "oauth2.timeout.connect";
	
	// RTG Request Properties
	public static final String ORIGIN_ACCESS_ID				= "rtg.digital.request.origin.access.id";
	public static final String ORIGIN_POS_ENTRY_MODE		= "rtg.digital.request.origin.pos.entry.mode";
	public static final String ORIGIN_RETAILER_NAME			= "rtg.digital.request.origin.retailer.name";
	public static final String ORIGIN_STORE_ID				= "rtg.digital.request.origin.store.id";
	public static final String ORIGIN_TERM_ID				= "rtg.digital.request.origin.term.id";
	
	// Fault Message Properties
	public static final String NOT_AVAILABLE_MESSAGE 					= "error.not.available";
	public static final String RTG_RESPONSE_TIMEOUT_MESSAGE				= "error.rtg.response.timeout";
	public static final String RTG_RESPONSE_CODE_MESSAGE 				= "error.rtg.response.code";
	public static final String RTG_RESPONSE_MAPPING_MISSING_MESSAGE 	= "error.rtg.response.mapping.missing";
	public static final String RTG_INVALID_URL_MESSAGE					= "error.rtg.invalid.url";
	
	// RTG Response Keys
	public static final String PIN_KEY 							= "rtg.response.key.pin";
	public static final String DEFAULT_PIN_KEY					= "rtg.response.key.pin.default";
	public static final String SERIAL_KEY						= "rtg.response.key.serial";
	public static final String DEFAULT_SERIAL_KEY				= "rtg.response.key.serial.default";
	
	// RTG Response Log Filters
	public static final String RTG_RESPONSE_FILTER_KEYS			= "filter.rtg.response.keys";
	
	// Obfuscation Filter Properties
	public static final String FILTER_MIN_HIDDEN				= "filter.hidden.min";
	public static final String FILTER_MAX_VISIBLE				= "filter.visible.max";
	
	// Exception Mapping
	// These are also valid names for ExceptionEnum and must remain so.
	public static final String INVALID_PRODUCT			= "InvalidProduct";
	public static final String INVALID_AMOUNT			= "InvalidAmount";
	public static final String VOUCHER_NOT_FOUND 		= "VoucherNotFound";
	public static final String NOT_SUPPORTED			= "NotSupported";
	public static final String TX_REF_NOT_FOUND			= "TxRefNotFound";
	public static final String VOUCHER_USED				= "VoucherUsed";
	
}
