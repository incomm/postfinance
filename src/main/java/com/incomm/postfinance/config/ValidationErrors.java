package com.incomm.postfinance.config;


public interface ValidationErrors {

	public static final String TX_ID_NOT_NULL = "Transaction IDs must not be null.";
	public static final String TX_ID_PATTERN = "Transaction IDs must contain alpha-numeric characters only.";
	public static final String TX_ID_SIZE = "Transaction IDs must be [1-40] characters.";
	
	public static final String PRODUCT_ID_NOT_NULL = "Product ID must not be null.";
	public static final String PRODUCT_ID_PATTERN = "Product ID must contain alpha-numeric characters only.";
	public static final String PRODUCT_ID_SIZE = "Product ID must be [1-40] characters.";
	
	public static final String SERIAL_NOT_NULL = "Serial Number must not be null.";
	public static final String SERIAL_PATTERN = "Serial Number must contain alpha-numeric characters only.";
	public static final String SERIAL_SIZE = "Serial Number must be [1-30] characters.";
	
	public static final String AMOUNT_NOT_NULL = "Purchase amount must not be null.";
	
	public static final String CURRENCY_NOT_NULL = "Currency code must not be null.";
	public static final String CURRENCY_PATTERN = "Currency code must contain alphabetical characters only.";
	public static final String CURRENCY_SIZE = "Currency code must be [3] characters.";
	
	public static final String AUTOMATIC_NOT_NULL = "Automatic must be true or false, it cannot be null.";
	
}
