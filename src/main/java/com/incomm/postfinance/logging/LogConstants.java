package com.incomm.postfinance.logging;

import org.apache.log4j.Level;

public interface LogConstants {

	// Success logging level (for easy access)
	public static final Level SUCCESS = Success.SUCCESS;
	
	// NOC logging labels
	public static String PP_CODE = "ppRespCode";
	public static String PP_TXID = "ppTxID";
	public static String PP_INVALID_RESPONSE = "ppRTGRespCode";
	public static String PP_URL = "ppURL";
	public static String PP_EXPECTED = "ppExpected";
	public static String PP_VALUE = "ppValue";
	public static String PP_CONFIG = "ppConfig";
	public static String PP_KEY = "ppKey";
	
	// NOC logging codes
	public static final int SUCCESS_CODE 						= 0;
	
	public static final int OAUTH2_NOT_AVAILABLE_CODE 			= 5001;
	public static final int OAUTH2_INVALID_CREDENTIALS_CODE 	= 5002;
	public static final int OAUTH2_CONNECT_TIMEOUT_CODE 		= 5003;
	public static final int OAUTH2_RECEIVE_TIMEOUT_CODE 		= 5004;
	public static final int OAUTH2_OTHER_ERROR_CODE 			= 5005;
	
	public static final int RTG_NOT_AVAILABLE_CODE 				= 5011;
	public static final int RTG_INVALID_URI 					= 5012;
	public static final int RTG_CONNECT_TIMEOUT_CODE 			= 5013;
	public static final int RTG_RECEIVE_TIMEOUT_CODE 			= 5014;
	public static final int RTG_OTHER_ERROR_CODE 				= 5015;
	public static final int RTG_INVALID_RESPONSE_CODE 			= 5016; 
	
	public static final int PROPERTIES_DECRYPTION_CODE 			= 5021;
	public static final int LOGGING_FILTERS_CODE 				= 5022;
	public static final int PIN_KEY_MAPPING_NOT_CONFIGURED		= 5023;
	public static final int PIN_KEY_MAPPING_FAILED				= 5024;
	public static final int SERIAL_KEY_MAPPING_NOT_CONFIGURED	= 5025;
	public static final int SERIAL_KEY_MAPPING_FAILED			= 5026;
}
