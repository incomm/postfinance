package com.incomm.postfinance.logging;

import org.apache.log4j.Level;


public class Success extends Level {

    public static final int SUCCESS_INT = Level.FATAL_INT + 10000;
    public static final Level SUCCESS = new Success(SUCCESS_INT, "SUCCESS", 7);

    protected Success(int arg0, String arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

    public static Level toLevel(String sArg) {
        if (sArg != null && sArg.toUpperCase().equals("SUCCESS")) {
            return SUCCESS;
        }
        return (Level) toLevel(sArg, Level.DEBUG);
    }

    public static Level toLevel(int val) {
        if (val == SUCCESS_INT) {
            return SUCCESS;
        }
        return (Level) toLevel(val, Level.DEBUG);
    }

    public static Level toLevel(int val, Level defaultLevel) {
        if (val == SUCCESS_INT) {
            return SUCCESS;
        }
        return Level.toLevel(val, defaultLevel);
    }

    public static Level toLevel(String sArg, Level defaultLevel) {
        if (sArg != null && sArg.toUpperCase().equals("SUCCESS")) {
            return SUCCESS;
        }
        return Level.toLevel(sArg, defaultLevel);
    }
}