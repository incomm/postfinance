package com.incomm.postfinance.rtg;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ws.rs.ProcessingException;

import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.rs.security.oauth2.client.OAuthClientUtils;
import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.cxf.rs.security.oauth2.grants.clientcred.ClientCredentialsGrant;
import org.apache.cxf.rs.security.oauth2.provider.OAuthServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.config.PostFinanceConfig;
import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.logging.LogConstants;


@Service(value="authProvider")
public class AuthProvider implements LogConstants {

	public Logger log = Logger.getLogger(AuthProvider.class);

	// OAuth2 Constants
	public static final String AUTHORIZATION 				= "Authorization";
	public static final String CLIENT_ID_KEY 				= "client_id";
	public static final String CLIENT_SECRET_KEY 			= "client_secret";
	public static final String SCOPE 						= "scope";

	public static final long AUTH_RECEIVE_TIMEOUT_DEFAULT 	= 8000L;
	public static final long AUTH_CONNECT_TIMEOUT_DEFAULT 	= 8000L;

	long receiveTimeout;
	long connectionTimeout;

	@Autowired PostFinanceConfig config;

	public AuthProvider() {

	}

	@PostConstruct
	public void postConstruct() {

		receiveTimeout = config.getPropertyAsLong(PropertyConstants.RTG_RECEIVE_TIMEOUT, AUTH_RECEIVE_TIMEOUT_DEFAULT);
		connectionTimeout = config.getPropertyAsLong(PropertyConstants.RTG_CONNECT_TIMEOUT, AUTH_CONNECT_TIMEOUT_DEFAULT);
	}

	/**
	 * Authenticates with the OAuth2 end-point determined by the configuration 
	 * 
	 * @return authenticated token
	 * @throws RTGException thrown in event that the OAuth2 server cannot be reached or a timeout occurs
	 */
	public ClientAccessToken getClientAccessToken() throws RTGException {

		ClientAccessToken accessToken = null;
		WebClient client = null;

		try {
			log.info("Authenticating at " + getOAuth2URL());
			Map<String, String> extMap = new HashMap<String, String>();
			extMap.put(CLIENT_ID_KEY, getClientId());
			extMap.put(CLIENT_SECRET_KEY, getClientSecret());
			extMap.put(SCOPE, getScope());
			client = WebClient.create(getOAuth2URL());
			WebClient.getConfig(client).getHttpConduit().getClient().setReceiveTimeout(receiveTimeout);
			WebClient.getConfig(client).getHttpConduit().getClient().setConnectionTimeout(connectionTimeout);
			accessToken = OAuthClientUtils.getAccessToken(client, new ClientCredentialsGrant(), extMap);

		} catch (Exception e) {
			throw new RTGException(e, getRespCode(e), getOAuth2URL());

		} finally {
			if (client != null) client.close();
		}

		return accessToken;
	}

	/**
	 * Determines NOC response code from an exception produced in a failed attempt to authenticate
	 * 
	 * @param e
	 * @return
	 */
	protected int getRespCode(Exception e) {

		if (e instanceof ProcessingException) {
			if (e.getCause() != null && e.getCause() instanceof ConnectException) {
				return OAUTH2_NOT_AVAILABLE_CODE;
			}
			else if (e.getCause() != null && e.getCause() instanceof SocketTimeoutException) {
				Throwable socketTimeout = e.getCause();
				if (socketTimeout.getCause() != null && socketTimeout.getCause().getMessage().contains(RTGUtil.READ_TIMEOUT)) {
					return OAUTH2_RECEIVE_TIMEOUT_CODE;
				} else if (socketTimeout.getCause() != null && socketTimeout.getCause().getMessage().contains(RTGUtil.CONNECT_TIMEOUT)) {
					return OAUTH2_CONNECT_TIMEOUT_CODE;
				} else if (socketTimeout.getCause() != null && socketTimeout.getCause() instanceof UnknownHostException) {
					return OAUTH2_NOT_AVAILABLE_CODE;
				}
			}
		}
		else if (e instanceof OAuthServiceException) {
			return OAUTH2_INVALID_CREDENTIALS_CODE;
		} 

		// Default exception for unforeseen issues
		return OAUTH2_OTHER_ERROR_CODE;
	}

	protected String getClientId() {

		return config.getProperty(PropertyConstants.OAUTH2_CLIENT_ID);
	}

	protected String getClientSecret() {

		return config.getProperty(PropertyConstants.OAUTH2_CLIENT_SECRET);
	}

	protected String getScope() {

		return config.getProperty(PropertyConstants.OAUTH2_SCOPE, "read write");
	}

	public String getOAuth2URL() {

		return config.getProperty(PropertyConstants.OAUTH2_URL);
	}

	public PostFinanceConfig getConfig() {
		return config;
	}

	public void setConfig(PostFinanceConfig config) {
		this.config = config;
	}

}
