
package com.incomm.postfinance.rtg;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory extends generated.ObjectFactory {

    private final static QName _RTGRequest_QNAME = new QName("", "RetailTransactionDigitalRequest");
    private final static QName _RTGResponse_QNAME = new QName("", "RetailTransactionDigitalResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RTGRequest }
     * 
     */
    public RTGRequest createRTGRequest() {
        return new RTGRequest();
    }

    /**
     * Create an instance of {@link RTGResponse }
     * 
     */
    public RTGResponse createRTGResponse() {
        return new RTGResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RTGRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RetailTransactionDigitalRequest")
    public JAXBElement<RTGRequest> createRTGRequest(RTGRequest value) {
        return new JAXBElement<RTGRequest>(_RTGRequest_QNAME, RTGRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RTGResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RetailTransactionDigitalResponse")
    public JAXBElement<RTGResponse> createRTGResponse(RTGResponse value) {
        return new JAXBElement<RTGResponse>(_RTGResponse_QNAME, RTGResponse.class, null, value);
    }

}
