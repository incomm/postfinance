package com.incomm.postfinance.rtg;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.annotation.PostConstruct;
import javax.naming.ConfigurationException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;

import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.rs.security.oauth2.client.OAuthClientUtils;
import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.config.PostFinanceConfig;
import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.logging.LogConstants;

@Service("rtgDigitalService")
public class RTGDigitalService implements LogConstants {

	private static final long RTG_RECEIVE_TIMEOUT_DEFAULT = 30000L;
	private static final long RTG_CONNECT_TIMEOUT_DEFAULT = 30000L;

	static Logger log = Logger.getLogger(RTGDigitalService.class);

	long receiveTimeout;
	long connectionTimeout;

	@Autowired PostFinanceConfig config;
	@Autowired RTGUtil rtgUtil;

	public RTGDigitalService() {

	}

	@PostConstruct
	public void init() throws ConfigurationException {

		receiveTimeout = getConfig().getPropertyAsLong(PropertyConstants.RTG_RECEIVE_TIMEOUT, RTG_RECEIVE_TIMEOUT_DEFAULT);
		connectionTimeout = getConfig().getPropertyAsLong(PropertyConstants.RTG_CONNECT_TIMEOUT, RTG_CONNECT_TIMEOUT_DEFAULT);

		try {
			getBuilder(getConfig().getProperty(PropertyConstants.RTG_PURCHASE_URL));
		} catch (RTGException e) {
			String message = getConfig().getProperty(PropertyConstants.RTG_INVALID_URL_MESSAGE);
			log.warn(message);
			log.fatal(PP_CODE + "=" + RTG_INVALID_URI + "|" + 
					PP_URL + "=" + getConfig().getProperty(PropertyConstants.RTG_PURCHASE_URL));

			// In production, throw the exception to prevent successful start-up
			if (config.getDeployment().equals(PropertyConstants.PRODUCTION)) {
				throw new ConfigurationException(message);
			}
		}
	}

	/**
	 * POSTs a request to the RTG end-point for purchases.
	 * 
	 * @param request request to POST
	 * @param token authentication token for the POST
	 * @return response from RTG
	 * @throws RTGException thrown if there is an issue connecting to the end-point or if a connect or receive timeout occurs.
	 * @see #post(RTGRequest, ClientAccessToken, javax.ws.rs.client.Invocation.Builder)
	 */
	public RTGResponse purchase(RTGRequest request, ClientAccessToken token) throws RTGException {

		// TODO: (Hari) should we auto-cancel on activate timeout?
		return post(request, token, config.getProperty(PropertyConstants.RTG_PURCHASE_URL));
	}

	/**
	 * POSTs a request to the RTG end-point for status queries.
	 * 
	 * @param request request to POST
	 * @param token authentication token for the POST
	 * @return response from RTG
	 * @throws RTGException thrown if there is an issue connecting to the end-point or if a connect or receive timeout occurs.
	 * @see #post(RTGRequest, ClientAccessToken, javax.ws.rs.client.Invocation.Builder)
	 */
	public RTGResponse getState(RTGRequest request, ClientAccessToken token) throws RTGException {

		return post(request, token, config.getProperty(PropertyConstants.RTG_GETSTATE_URL));
	}

	/**
	 * POSTs a request to the RTG end-point for cancellations.
	 * 
	 * @param request request to POST
	 * @param token authentication token for the POST
	 * @return response from RTG
	 * @throws RTGException thrown if there is an issue connecting to the end-point or if a connect or receive timeout occurs.
	 * @see #post(RTGRequest, ClientAccessToken, javax.ws.rs.client.Invocation.Builder)
	 */
	public RTGResponse cancel(RTGRequest request, ClientAccessToken token) throws RTGException {

		return post(request, token, config.getProperty(PropertyConstants.RTG_CANCEL_URL));
	}

	/**
	 * Constructs a RESTful end-point for communicating with using XML formatted messages.
	 * 
	 * @param key Property key which contains the end-point's URL
	 * @return RESTful end-point expecteding XML formatted messages.
	 * @throws RTGException thrown if the end-point URL is null or invalid
	 */
	protected Invocation.Builder getBuilder(String url) throws RTGException {

		if (url == null) {
			throw new RTGException(RTG_INVALID_URI, url);
		}

		try {
			@SuppressWarnings("static-access")
			Client client = ClientBuilder.newBuilder().newClient();
			WebTarget target = client.target(url).queryParam("format", "xml");
			Invocation.Builder builder = target.request();
			WebClient.getConfig(builder).getHttpConduit().getClient().setReceiveTimeout(receiveTimeout);
			WebClient.getConfig(builder).getHttpConduit().getClient().setConnectionTimeout(connectionTimeout);
			return builder;

		} catch (IllegalArgumentException e) {
			throw new RTGException(e, RTG_INVALID_URI, url);
		}
	}

	/**
	 * Performs a POST operation.
	 * 
	 * @param request message to be POSTed
	 * @param token authentication token for the POST
	 * @param builder end-point to POST to
	 * @return response from the service at the given end-point.
	 * @throws RTGException thrown if there is an issue connecting to the end-point or if a connect or receive timeout occurs.
	 */
	protected RTGResponse post(RTGRequest request, ClientAccessToken token, String url) throws RTGException {

		try {
			Invocation.Builder builder = getBuilder(url);
			String authHeader = OAuthClientUtils.createAuthorizationHeader(token);
			builder.header(AuthProvider.AUTHORIZATION, authHeader);
			log.info("RTG Request : " + url + "\n" + rtgUtil.marshal(request));
			RTGResponse response = builder.post(Entity.xml(request), RTGResponse.class);
			log.info("RTG Response:\n" + rtgUtil.marshal(response));
			return response;

		} catch (Exception e) {
			throw new RTGException(e, getRespCode(e), url);
		}
	}

	/**
	 * Determines the NOC ppRespCode based on the provided Exception.
	 *  
	 * @param e
	 * @return
	 */
	protected int getRespCode(Exception e) {

		if (e instanceof ProcessingException) {
			if (e.getCause() != null && e.getCause() instanceof ConnectException) {
				return RTG_NOT_AVAILABLE_CODE;
			}
			else if (e.getCause() != null && e.getCause() instanceof SocketTimeoutException) {
				Throwable socketTimeout = e.getCause();
				if (socketTimeout.getCause() != null && socketTimeout.getCause().getMessage().contains(RTGUtil.READ_TIMEOUT)) {
					return RTG_RECEIVE_TIMEOUT_CODE;
				} else if (socketTimeout.getCause() != null && socketTimeout.getCause().getMessage().contains(RTGUtil.CONNECT_TIMEOUT)) {
					return RTG_CONNECT_TIMEOUT_CODE;
				} else if (socketTimeout.getCause() != null && socketTimeout.getCause() instanceof UnknownHostException) {
					return RTG_NOT_AVAILABLE_CODE;
				}
			}
		}

		// Default exception for unforeseen issues
		return RTG_OTHER_ERROR_CODE;
	}

	public PostFinanceConfig getConfig() {
		return config;
	}

	public void setConfig(PostFinanceConfig config) {
		this.config = config;
	}

	public RTGUtil getRtgUtil() {
		return rtgUtil;
	}

	public void setRtgUtil(RTGUtil rtgUtil) {
		this.rtgUtil = rtgUtil;
	}

}
