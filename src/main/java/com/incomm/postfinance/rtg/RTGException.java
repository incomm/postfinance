package com.incomm.postfinance.rtg;

@SuppressWarnings("serial")
public class RTGException extends Exception {
	
	String url;
	int ppRespCode;

	public RTGException() {
	}

	public RTGException(String message) {
		super(message);
	}

	public RTGException(Throwable cause) {
		super(cause);
	}
	
	public RTGException(int ppRespCode) {
		setRespCode(ppRespCode);
	}
	
	public RTGException(Throwable cause, int ppRespCode) {
		super(cause);
		setRespCode(ppRespCode);
	}
	
	public RTGException(Throwable cause, int ppRespCode, String url) {
		super(cause);
		setRespCode(ppRespCode);
		setUrl(url);
	}

	public RTGException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public RTGException(String message, Throwable cause, int ppRespCode) {
		super(message, cause);
		setRespCode(ppRespCode);
	}

	public RTGException(int ppRespCode, String url) {
		super();
		setRespCode(ppRespCode);
		setUrl(url);
	}

	public int getRespCode() {
		return ppRespCode;
	}

	public void setRespCode(int ppRespCode) {
		this.ppRespCode = ppRespCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
