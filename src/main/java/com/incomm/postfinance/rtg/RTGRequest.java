/**
 * 
 */
package com.incomm.postfinance.rtg;

import generated.RetailTransactionRequest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RetailTransactionDigitalRequest")
public class RTGRequest extends RetailTransactionRequest {

}
