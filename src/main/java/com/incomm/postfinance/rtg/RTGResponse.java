
package com.incomm.postfinance.rtg;

import generated.RetailTransactionResponse;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RetailTransactionDigitalResponse")
public class RTGResponse extends RetailTransactionResponse
{

}
