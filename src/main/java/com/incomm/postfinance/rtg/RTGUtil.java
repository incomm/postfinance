package com.incomm.postfinance.rtg;

import generated.MetaField;
import generated.ObjectFactory;
import generated.Origin;
import generated.ProductRequest;
import generated.ProductRequest.Metafields;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.naming.ConfigurationException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;

import com.incomm.postfinance.config.PostFinanceConfig;
import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.logging.LogConstants;
import com.incomm.postfinance.utils.DateUtil;
import com.incomm.postfinance.utils.StringUtil;

@Service(value="rtgUtil")
public class RTGUtil implements LogConstants {

	public Logger log = Logger.getLogger(RTGUtil.class);

	public static String READ_TIMEOUT = "Read timed out";
	public static String CONNECT_TIMEOUT = "connect timed out";
	
	public static String HARDCODED_DEFAULT_PIN_KEY = "pin";
	public static String HARDCODED_DEFAULT_SERIAL_KEY = "serialNum";

	@Autowired PostFinanceConfig config;
	@Autowired StringUtil stringUtil;

	ObjectFactory factory = null;
	JAXBContext context = null;

	HashSet<String> responseFilters = null;

	public RTGUtil() throws JAXBException {

		factory = new ObjectFactory();
		context = JAXBContext.newInstance("com.incomm.postfinance.rtg");
	}

	@PostConstruct
	public void init() throws ConfigurationException {

		// Read Response Filters from Configuration
		String[] filters = config.getPropertyAsStringArray(PropertyConstants.RTG_RESPONSE_FILTER_KEYS);
		if (filters != null) {
			StringBuilder filterList = new StringBuilder("RTGResponse logging filters: ");
			for (String filter : filters) {
				filterList.append(filter + " ");
			}
			log.info(filterList.toString());
			responseFilters = new HashSet<String>(Arrays.asList(filters));
		} else {
			log.warn("No RTGResponse logging filters detected - sensitive information will be logged.");
			log.fatal(PP_CODE + "=" + LOGGING_FILTERS_CODE);

			// In production, throw the exception to prevent successful start-up
			if (config.getDeployment().equals(PropertyConstants.PRODUCTION)) {
				throw new ConfigurationException("No RTGResponse logging filters detected - sensitive information would be logged.");
			}
		}
	}

	public String marshal(Object object) throws JAXBException {

		// Each call creates a new marshaller = thread safety
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		marshaller.setListener(new Marshaller.Listener() {

			HashMap<String, String> fields = new HashMap<String, String>();
			String code = null;

			@Override
			public void afterMarshal(Object object) {
				if(object instanceof RTGResponse) {
					RTGResponse response = (RTGResponse) object;
					
					if (checkCode(response)) {
						response.getProduct().setCode(code);
					}
					
					if (checkFields(response)) {
						for (MetaField field : response.getProduct().getMetafields().getMetafield()) {
							if (fields.containsKey(field.getName())) { // field was filtered
								field.setValue(fields.get(field.getName()));
							}
						}
					}
				}
				
				else if(object instanceof RTGRequest) {
					RTGRequest request = (RTGRequest) object;
					
					if (checkCode(request)) {
						request.getProduct().setCode(code);
					}
					
					if (checkFields(request)) {
						for (MetaField field : request.getProduct().getMetafields().getMetafield()) {
							if (fields.containsKey(field.getName())) { // field was filtered
								field.setValue(fields.get(field.getName()));
							}
						}
					}
				}
			}

			// This method is invoked twice for some reason, 
			// protected against data loss by checking for existing value mapping and nullity
			@Override
			public void beforeMarshal(Object object) {
				if(object instanceof RTGResponse) {
					RTGResponse response = (RTGResponse) object;

					if (checkCode(response) && code == null) {
						code = response.getProduct().getCode();
						response.getProduct().setCode(stringUtil.obfuscate(response.getProduct().getCode()));
					}
					
					if (checkFields(response)) {
						for (MetaField field : response.getProduct().getMetafields().getMetafield()) {
							if (responseFilters.contains(field.getName())) { // filtering this field
								if (!fields.containsKey(field.getName())) { // not already filtered
									fields.put(field.getName(), field.getValue());
									field.setValue(stringUtil.obfuscate(field.getValue()));
								}
							}
						}
					}
				}
				
				else if(object instanceof RTGRequest) {
					RTGRequest request = (RTGRequest) object;

					if (checkCode(request) && code == null) {
						code = request.getProduct().getCode();
						request.getProduct().setCode(stringUtil.obfuscate(request.getProduct().getCode()));
					}
					
					if (checkFields(request)) {
						for (MetaField field : request.getProduct().getMetafields().getMetafield()) {
							if (responseFilters.contains(field.getName())) { // filtering this field
								if (!fields.containsKey(field.getName())) { // not already filtered
									fields.put(field.getName(), field.getValue());
									field.setValue(stringUtil.obfuscate(field.getValue()));
								}
							}
						}
					}
				}
			}

			private boolean checkFields(RTGResponse response) {
				if (response == null || response.getProduct() == null || response.getProduct().getMetafields() == null) {
					return false;
				}
				if (responseFilters == null) {
					return false;
				}
				return true;
			}

			private boolean checkCode(RTGResponse response) {
				if (response == null || response.getProduct() == null) {
					return false;
				}
				if (responseFilters == null || !responseFilters.contains("code")) {
					return false;
				}
				return true;
			}
			
			private boolean checkFields(RTGRequest request) {
				if (request == null || request.getProduct() == null || request.getProduct().getMetafields() == null) {
					return false;
				}
				if (responseFilters == null) {
					return false;
				}
				return true;
			}

			private boolean checkCode(RTGRequest request) {
				if (request == null || request.getProduct() == null) {
					return false;
				}
				if (responseFilters == null || !responseFilters.contains("code")) {
					return false;
				}
				return true;
			}
		});

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		marshaller.marshal(object, stream);
		return stream.toString();
	}

	public RTGRequest getDigitalRequest() {

		RTGRequest req = new RTGRequest();
		req.setDateTime(DateUtil.format(Calendar.getInstance()));

		// Populates generic Product
		ProductRequest product = getRequest();
		req.setProduct(product);

		// Populates merchant-specific Origin
		Origin origin = getOrigin();
		req.setOrigin(origin);

		return req;
	}

	public Origin getOrigin() {

		Origin origin = new Origin();
		origin.setAccessID(config.getProperty(PropertyConstants.ORIGIN_ACCESS_ID));
		origin.setPosEntryMode(config.getProperty(PropertyConstants.ORIGIN_POS_ENTRY_MODE));
		origin.setRetailerName(config.getProperty(PropertyConstants.ORIGIN_RETAILER_NAME));
		origin.setStoreID(config.getProperty(PropertyConstants.ORIGIN_STORE_ID));
		origin.setTermID(config.getProperty(PropertyConstants.ORIGIN_TERM_ID));
		return origin;
	}

	public ProductRequest getRequest() {

		ProductRequest product = new ProductRequest();
		product.setCode(null);
		product.setMetafields(new Metafields());
		product.setTcVersion(null);
		product.setTrack1(null);
		product.setTrack2(null);
		return product;
	}

	public MetaField findResponseField(generated.ProductResponse.Metafields fields, String key) {

		for (MetaField field : fields.getMetafield()) {
			if (field.getName() != null && field.getName().equals(key))
				return field;
		}
		return null;
	}

	public MetaField findResponseField(RTGResponse response, String key) {

		return findResponseField(response.getProduct().getMetafields(), key);
	}

	public String findResponseFieldValue(RTGResponse response, String key) {

		MetaField field = findResponseField(response, key);
		if (field == null) {
			return null;
		} else {
			return field.getValue();
		}

	}

	private String getPinKey(String upc) {
		
		String key = config.getProperty(PropertyConstants.PIN_KEY + "." + upc);
		
		if (key == null) {
			key = config.getProperty(PropertyConstants.DEFAULT_PIN_KEY);
		}
		
		if (key == null) {
			key = HARDCODED_DEFAULT_PIN_KEY;
		}
		
		return key;
	}
	
	public String getPin(RTGResponse response, String upc) {
		
		String key = getPinKey(upc);		
		String value = findResponseFieldValue(response, key);
		
		if (value == null) {
			logFailedMapping(PIN_KEY_MAPPING_FAILED, key);
		}
		
		return value;
	}
	
	private String getSerialKey(String upc) {
		
		String key = config.getProperty(PropertyConstants.SERIAL_KEY + "." + upc);
		
		if (key == null) {
			key = config.getProperty(PropertyConstants.DEFAULT_SERIAL_KEY);
		}
		
		if (key == null) {
			key = HARDCODED_DEFAULT_SERIAL_KEY;
		}
		
		return key;
	}
	
	public String getSerial(RTGResponse response, String upc) {
		
		String key = getSerialKey(upc);		
		String value = findResponseFieldValue(response, key);
		
		if (value == null) {
			logFailedMapping(SERIAL_KEY_MAPPING_FAILED, key);
		}
		
		return value;
	}
	
	private void logFailedMapping(int respCode, String value) {
		
		log.fatal(PP_CODE + "=" + respCode + "|" + PP_KEY + "=" + value);
	}

	public PostFinanceConfig getConfig() {
		return config;
	}

	public void setConfig(PostFinanceConfig config) {
		this.config = config;
	}

	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}
	
}
