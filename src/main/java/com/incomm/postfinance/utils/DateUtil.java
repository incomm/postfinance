package com.incomm.postfinance.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class DateUtil {

	protected static SimpleDateFormat formatter; 
	protected static DatatypeFactory factory;
	
	static {
		formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		formatter.setTimeZone(TimeZone.getTimeZone("Zulu"));
		
		try {
			factory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	public static String format(Calendar calendar) {
		
		return format(calendar.getTime());
	}
	
	public static String format(Date date) {
		
		return formatter.format(date);
	}
	
	public static XMLGregorianCalendar getCurrentZuluTime() {
	
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Zulu"));
		GregorianCalendar greg = new GregorianCalendar();
		greg.setTime(cal.getTime());
		return factory.newXMLGregorianCalendar(greg);
	}

}
