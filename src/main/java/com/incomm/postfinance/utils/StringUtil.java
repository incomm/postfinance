package com.incomm.postfinance.utils;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.config.PostFinanceConfig;
import com.incomm.postfinance.config.PropertyConstants;

@Service(value="stringUtil")
public class StringUtil {

	@Autowired PostFinanceConfig config;
	
	// Default values overridden by configuration
	int minObufscated = 4;
    int maxVisible = 8;
	
	public StringUtil() {
	
	}
	
	@PostConstruct
	public void init() {
		
		minObufscated = config.getPropertyAsInt(PropertyConstants.FILTER_MIN_HIDDEN, minObufscated);
		maxVisible = config.getPropertyAsInt(PropertyConstants.FILTER_MAX_VISIBLE, maxVisible);
	}
    
    public String obfuscate(String value) {
    	
    	if (value == null) return null;
    	
    	// Determine how much to keep visible
    	int visible = value.length() - minObufscated;
    	if (visible > maxVisible) visible = maxVisible;
    	int leftVisible = visible / 2;
    	int rightVisible = visible - leftVisible;
    	
    	StringBuilder output = new StringBuilder();
    	
    	// Append left-visible portion
    	output.append(value.substring(0, leftVisible));
    	
    	// Append middle-obfuscated portion
    	char[] middle = new char[value.length() - visible];
    	for (int i = 0; i < middle.length; i++) {
    		middle[i] = '*';
    	}
    	output.append(middle);
    	
    	// Append right-visible portion
    	output.append(value.substring(value.length() - rightVisible));
    	
    	return output.toString();
    }

}
