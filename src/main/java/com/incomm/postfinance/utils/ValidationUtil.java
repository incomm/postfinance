package com.incomm.postfinance.utils;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.stereotype.Service;

@Service("validationUtil")
public class ValidationUtil {
	
	private Validator validator;
	
	public ValidationUtil() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	/**
	 * Performs validation on an annotated object.
	 * 
	 * @param object Annotated object to validate.
	 * @throws ValidationException Thrown if bean fails validation.
	 */
	public <T> void validate(T object) throws ValidationException {
		
		if (object == null) throw new ValidationException("Request object is null.");
		Set<ConstraintViolation<T>> violations = validator.validate(object);
		
		if (violations.size() > 0) {
			StringBuilder sb = new StringBuilder();
			for (ConstraintViolation<T> violation : violations) {
				sb.append(violation.getMessage() + " ");
			}
			throw new ValidationException(sb.toString());
		}
	}

}
