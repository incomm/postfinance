package com.incomm.postfinance.voucher.fault;

import javax.xml.ws.WebFault;

@WebFault(name="FatalInteralFault")
public class FatalInternalFault extends Exception {

	public FatalInternalFault() {
		
	}

	public FatalInternalFault(String message) {
		super(message);
	}

	public FatalInternalFault(Throwable cause) {
		super(cause);
	}

	public FatalInternalFault(String message, Throwable cause) {
		super(message, cause);
	}

}
