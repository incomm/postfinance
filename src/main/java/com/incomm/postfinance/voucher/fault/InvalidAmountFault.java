package com.incomm.postfinance.voucher.fault;

import javax.xml.ws.WebFault;

@WebFault(name="InvalidAmount")
public class InvalidAmountFault extends Exception {

	public InvalidAmountFault() {
	}

	public InvalidAmountFault(String message) {
		super(message);
	}

	public InvalidAmountFault(Throwable cause) {
		super(cause);
	}

	public InvalidAmountFault(String message, Throwable cause) {
		super(message, cause);
	}

}
