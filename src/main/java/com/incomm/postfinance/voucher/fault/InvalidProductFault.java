package com.incomm.postfinance.voucher.fault;

import javax.xml.ws.WebFault;

@WebFault(name="InvalidProduct")
public class InvalidProductFault extends Exception {

	public InvalidProductFault() {
	}

	public InvalidProductFault(String message) {
		super(message);
	}

	public InvalidProductFault(Throwable cause) {
		super(cause);
	}

	public InvalidProductFault(String message, Throwable cause) {
		super(message, cause);
	}

}
