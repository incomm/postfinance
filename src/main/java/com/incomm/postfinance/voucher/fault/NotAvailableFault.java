package com.incomm.postfinance.voucher.fault;

import javax.xml.ws.WebFault;

@WebFault(name="NotAvailable")
public class NotAvailableFault extends Exception {

	public NotAvailableFault() {
	}

	public NotAvailableFault(String message) {
		super(message);
	}

	public NotAvailableFault(Throwable cause) {
		super(cause);
	}

	public NotAvailableFault(String message, Throwable cause) {
		super(message, cause);
	}

}
