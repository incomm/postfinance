package com.incomm.postfinance.voucher.fault;

import javax.xml.ws.WebFault;

@WebFault(name="NotSupported")
public class NotSupportedFault extends Exception {

	public NotSupportedFault() {
		
	}

	public NotSupportedFault(String message) {
		super(message);
	}

	public NotSupportedFault(Throwable cause) {
		super(cause);
	}

	public NotSupportedFault(String message, Throwable cause) {
		super(message, cause);
	}

}
