package com.incomm.postfinance.voucher.fault;

import javax.xml.ws.WebFault;

@WebFault(name="OtherPostFinanceFault")
public class OtherPostFinanceFault extends Exception {

	public OtherPostFinanceFault() {
	}

	public OtherPostFinanceFault(String message) {
		super(message);
	}

	public OtherPostFinanceFault(Throwable cause) {
		super(cause);
	}

	public OtherPostFinanceFault(String message, Throwable cause) {
		super(message, cause);
	}

}
