package com.incomm.postfinance.voucher.fault;

import javax.xml.ws.WebFault;

@WebFault(name="TxRefNotFound")
public class TxRefNotFoundFault extends Exception {

	public TxRefNotFoundFault() {
	}

	public TxRefNotFoundFault(String message) {
		super(message);
	}

	public TxRefNotFoundFault(Throwable cause) {
		super(cause);
	}

	public TxRefNotFoundFault(String message, Throwable cause) {
		super(message, cause);
	}

}
