package com.incomm.postfinance.voucher.fault;

import javax.xml.ws.WebFault;

@WebFault(name="VoucherNotFound")
public class VoucherNotFoundFault extends Exception {

	public VoucherNotFoundFault() {
	}

	public VoucherNotFoundFault(String message) {
		super(message);
	}

	public VoucherNotFoundFault(Throwable cause) {
		super(cause);
	}

	public VoucherNotFoundFault(String message, Throwable cause) {
		super(message, cause);
	}

}
