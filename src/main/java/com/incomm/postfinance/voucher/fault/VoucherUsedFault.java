package com.incomm.postfinance.voucher.fault;

import javax.xml.ws.WebFault;

@WebFault(name="VoucherUsed")
public class VoucherUsedFault extends Exception {

	public VoucherUsedFault() {
	}

	public VoucherUsedFault(String message) {
		super(message);
	}

	public VoucherUsedFault(Throwable cause) {
		super(cause);
	}

	public VoucherUsedFault(String message, Throwable cause) {
		super(message, cause);
	}

}
