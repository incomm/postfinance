package com.incomm.postfinance.voucher.message;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.incomm.postfinance.config.ValidationErrors;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelVoucherRequestVO", propOrder = {"txID","productID", "txRef", "automatic"})
public class CancelVoucherRequest implements Serializable {
	
	@NotNull(message=ValidationErrors.TX_ID_NOT_NULL)
	@Pattern(regexp="^[a-zA-Z0-9]*$", message=ValidationErrors.TX_ID_PATTERN)
	@Size(min=1, max=40, message=ValidationErrors.TX_ID_SIZE)
	@Valid
	@XmlElement(required=true)
	String txID;
	
	// RTG doesnt need UPC to perform Cancel
//	@NotNull(message=ValidationErrors.PRODUCT_ID_NOT_NULL)
//	@Pattern(regexp="^[a-zA-Z0-9]*$", message=ValidationErrors.PRODUCT_ID_PATTERN)
//	@Size(min=1, max=40, message=ValidationErrors.PRODUCT_ID_SIZE)
//	@Valid
	@XmlElement(required=true)
	String productID = null;
	
	@NotNull(message=ValidationErrors.TX_ID_NOT_NULL)
	@Pattern(regexp="^[a-zA-Z0-9]*$", message=ValidationErrors.TX_ID_PATTERN)
	@Size(min=1, max=40, message=ValidationErrors.TX_ID_SIZE)
	@Valid
	@XmlElement(required=true)
	String txRef = null;
	
//	@NotNull // Does not indicate in the spec provided that it must be not-null
//	@Valid
    @XmlElement(required=true)
	Boolean automatic = null;
	
	public CancelVoucherRequest() {
		
	}
	
	public String getTxID() {
		return txID;
	}

	public void setTxID(String txID) {
		this.txID = txID;
	}

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productId) {
		this.productID = productId;
	}

	public String getTxRef() {
		return txRef;
	}

	public void setTxRef(String txRef) {
		this.txRef = txRef;
	}

	public Boolean getAutomatic() {
		return automatic;
	}

	public void setAutomatic(Boolean automatic) {
		this.automatic = automatic;
	}

}
