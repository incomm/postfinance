package com.incomm.postfinance.voucher.message;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelVoucherResponseVO")
public class CancelVoucherResponse implements Serializable {

	public CancelVoucherResponse() {
		
	}

}
