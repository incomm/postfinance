package com.incomm.postfinance.voucher.message;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.incomm.postfinance.config.ValidationErrors;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetStateRequestVO", propOrder = {"txID", "txRef", "serial"})
public class GetStateRequest implements Serializable {
	
	@NotNull(message=ValidationErrors.TX_ID_NOT_NULL)
	@Pattern(regexp="^[a-zA-Z0-9]*$", message=ValidationErrors.TX_ID_PATTERN)
	@Size(min=1, max=40, message=ValidationErrors.TX_ID_SIZE)
	@Valid
	@XmlElement(required=true)
	String txID;
	
	@NotNull(message=ValidationErrors.TX_ID_NOT_NULL)
	@Pattern(regexp="^[a-zA-Z0-9]*$", message=ValidationErrors.TX_ID_PATTERN)
	@Size(min=1, max=40, message=ValidationErrors.TX_ID_SIZE)
	@Valid
	@XmlElement(required=true)
	String txRef = null;
	
	// RTG doesn't need serial number to perform LookupCode
//	@NotNull(message=ValidationErrors.SERIAL_NOT_NULL)
//	@Pattern(regexp="^[a-zA-Z0-9]*$", message=ValidationErrors.SERIAL_PATTERN)
//	@Size(min=1, max=30, message=ValidationErrors.SERIAL_SIZE)
//	@Valid
	@XmlElement(required=true)
	String serial = null;

	public GetStateRequest() {
		
	}
	
	public String getTxID() {
		return txID;
	}

	public void setTxID(String txID) {
		this.txID = txID;
	}
	
	public String getTxRef() {
		return txRef;
	}

	public void setTxRef(String txRef) {
		this.txRef = txRef;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

}
