package com.incomm.postfinance.voucher.message;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PingRequestVO", propOrder = {"txID"})
public class PingRequest implements Serializable {

	@XmlElement(required=true)
	String txID;

	public PingRequest() {
		
	}
	
	public String getTxID() {
		return txID;
	}

	public void setTxID(String txID) {
		this.txID = txID;
	}

}
