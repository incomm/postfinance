package com.incomm.postfinance.voucher.message;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.incomm.postfinance.config.ValidationErrors;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseVoucherRequestVO", propOrder = {"txID","productID", "amount", "currency"})
public class PurchaseVoucherRequest implements Serializable {
	
	@NotNull(message=ValidationErrors.TX_ID_NOT_NULL)
	@Pattern(regexp="^[a-zA-Z0-9]*$", message=ValidationErrors.TX_ID_PATTERN)
	@Size(min=1, max=40, message=ValidationErrors.TX_ID_SIZE)
	@Valid
	@XmlElement(required=true)
	String txID;
	
	@NotNull(message=ValidationErrors.PRODUCT_ID_NOT_NULL)
	@Pattern(regexp="^[a-zA-Z0-9]*$", message=ValidationErrors.PRODUCT_ID_PATTERN)
	@Size(min=1, max=40, message=ValidationErrors.PRODUCT_ID_SIZE)
	@Valid
	@XmlElement(required=true)
	String productID = null;
	
	@NotNull(message=ValidationErrors.AMOUNT_NOT_NULL)
	@Valid
	@XmlElement(required=true)
	BigDecimal amount = null;
	
	@NotNull(message=ValidationErrors.CURRENCY_NOT_NULL)
	@Pattern(regexp="^[A-Z][A-Z][A-Z]$", message=ValidationErrors.CURRENCY_PATTERN)
	@Size(min=3, max=3, message=ValidationErrors.CURRENCY_SIZE)
	@Valid
	@XmlElement(required=true)
	String currency = null;
	
	public PurchaseVoucherRequest() {
		
	}
	
	public String getTxID() {
		return txID;
	}

	public void setTxID(String txID) {
		this.txID = txID;
	}

	public String getProductID() {
		return productID;
	}

	public void setProductID(String productId) {
		this.productID = productId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
