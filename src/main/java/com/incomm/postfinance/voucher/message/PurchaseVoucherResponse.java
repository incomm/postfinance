package com.incomm.postfinance.voucher.message;

import java.io.Serializable;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseVoucherResponseVO", propOrder = {"pin", "serial", "valid"})
public class PurchaseVoucherResponse implements Serializable {
	
	@NotNull
	@Pattern(regexp="^[a-zA-Z0-9]*$")
	@Size(min=1,max=30)
	@Valid
	@XmlElement(required=true)
	String pin = null;
	
	@NotNull
	@Pattern(regexp="^[a-zA-Z0-9]*$")
	@Size(min=1,max=30)
	@Valid
	@XmlElement(required=true)
	String serial = null;
	 
	// Optional - no validation
	Date valid = null;

	public PurchaseVoucherResponse() {
		
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public Date getValid() {
		return valid;
	}

	public void setValid(Date valid) {
		this.valid = valid;
	}

}
