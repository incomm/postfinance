package com.incomm.postfinance.voucher.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.validation.ValidationException;
import javax.xml.bind.annotation.XmlElement;

import com.incomm.postfinance.voucher.fault.FatalInternalFault;
import com.incomm.postfinance.voucher.fault.InvalidAmountFault;
import com.incomm.postfinance.voucher.fault.InvalidProductFault;
import com.incomm.postfinance.voucher.fault.NotAvailableFault;
import com.incomm.postfinance.voucher.fault.NotSupportedFault;
import com.incomm.postfinance.voucher.fault.OtherPostFinanceFault;
import com.incomm.postfinance.voucher.fault.TxRefNotFoundFault;
import com.incomm.postfinance.voucher.fault.VoucherNotFoundFault;
import com.incomm.postfinance.voucher.fault.VoucherUsedFault;
import com.incomm.postfinance.voucher.message.CancelVoucherRequest;
import com.incomm.postfinance.voucher.message.CancelVoucherResponse;
import com.incomm.postfinance.voucher.message.GetStateRequest;
import com.incomm.postfinance.voucher.message.GetStateResponse;
import com.incomm.postfinance.voucher.message.PingRequest;
import com.incomm.postfinance.voucher.message.PingResponse;
import com.incomm.postfinance.voucher.message.PurchaseVoucherRequest;
import com.incomm.postfinance.voucher.message.PurchaseVoucherResponse;

@WebService
public interface PostFinanceIF {
	
	@WebMethod(operationName="PurchaseVoucher")
	@XmlElement(required=true)
	@WebResult(name="PurchaseVoucherResponse")
    public PurchaseVoucherResponse purchase(
			@XmlElement(required=true)
			@WebParam(name="PurchaseVoucherRequest")
    		PurchaseVoucherRequest request)
    		throws InvalidProductFault, InvalidAmountFault, NotAvailableFault, OtherPostFinanceFault, ValidationException, FatalInternalFault;
	
	@WebMethod(operationName="GetState")
	@XmlElement(required=true)
	@WebResult(name="GetStateResponse")
    public GetStateResponse getState(
			@XmlElement(required=true)
			@WebParam(name="GetStateRequest")
    		GetStateRequest request)
    		throws VoucherNotFoundFault, NotAvailableFault, NotSupportedFault, OtherPostFinanceFault, ValidationException, FatalInternalFault;
	
	@WebMethod(operationName="CancelVoucher")
	@XmlElement(required=true)
	@WebResult(name="CancelVoucherResponse")
    public CancelVoucherResponse cancel(
			@XmlElement(required=true)
			@WebParam(name="CancelVoucherRequest")
    		CancelVoucherRequest request)
    		throws VoucherUsedFault, TxRefNotFoundFault, NotAvailableFault, OtherPostFinanceFault, ValidationException, FatalInternalFault;
	
	@WebMethod(operationName="Ping")
	@XmlElement(required=true)
	@WebResult(name="PingResponse")
    public PingResponse ping(
			@XmlElement(required=true)
			@WebParam(name="PingRequest")
    		PingRequest request);

}
