package com.incomm.postfinance.voucher.service;

import generated.MetaField;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import javax.jws.WebService;
import javax.validation.ValidationException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.incomm.postfinance.logging.LogConstants;
import com.incomm.postfinance.rtg.RTGResponse;
import com.incomm.postfinance.utils.StringUtil;
import com.incomm.postfinance.utils.ValidationUtil;
import com.incomm.postfinance.voucher.fault.FatalInternalFault;
import com.incomm.postfinance.voucher.fault.InvalidAmountFault;
import com.incomm.postfinance.voucher.fault.InvalidProductFault;
import com.incomm.postfinance.voucher.fault.NotAvailableFault;
import com.incomm.postfinance.voucher.fault.NotSupportedFault;
import com.incomm.postfinance.voucher.fault.OtherPostFinanceFault;
import com.incomm.postfinance.voucher.fault.TxRefNotFoundFault;
import com.incomm.postfinance.voucher.fault.VoucherNotFoundFault;
import com.incomm.postfinance.voucher.fault.VoucherUsedFault;
import com.incomm.postfinance.voucher.message.CancelVoucherRequest;
import com.incomm.postfinance.voucher.message.CancelVoucherResponse;
import com.incomm.postfinance.voucher.message.GetStateRequest;
import com.incomm.postfinance.voucher.message.GetStateResponse;
import com.incomm.postfinance.voucher.message.PingRequest;
import com.incomm.postfinance.voucher.message.PingResponse;
import com.incomm.postfinance.voucher.message.PurchaseVoucherRequest;
import com.incomm.postfinance.voucher.message.PurchaseVoucherResponse;
import com.incomm.postfinance.workflow.CancelVoucherWorkflow;
import com.incomm.postfinance.workflow.GetStateWorkflow;
import com.incomm.postfinance.workflow.PingWorkflow;
import com.incomm.postfinance.workflow.PurchaseVoucherWorkflow;

@WebService(
		endpointInterface="com.incomm.postfinance.voucher.service.PostFinanceIF",
		portName="PostFinanceIFPort",
		serviceName="PostFinanceIFService")
public class PostFinanceImpl implements PostFinanceIF, LogConstants {

	private static Logger log = Logger.getLogger(PostFinanceImpl.class);

	@Autowired PurchaseVoucherWorkflow purchaseVoucherWorkflow;
	@Autowired GetStateWorkflow getStateWorkflow;
	@Autowired CancelVoucherWorkflow cancelVoucherWorkflow;
	@Autowired PingWorkflow pingWorkflow;

	@Autowired ValidationUtil validationUtil;
	@Autowired StringUtil stringUtil;

	public PostFinanceImpl() throws JAXBException {

	}

	public PurchaseVoucherResponse purchase(PurchaseVoucherRequest request) 
			throws InvalidProductFault, InvalidAmountFault, NotAvailableFault, OtherPostFinanceFault, ValidationException, FatalInternalFault {

		log.info(serialize(request));
		PurchaseVoucherResponse response = null;

		try {
			getValidationUtil().validate(request);
			response = getPurchaseVoucherWorkflow().execute(request);

		} catch (NotAvailableFault | InvalidProductFault | InvalidAmountFault | OtherPostFinanceFault | FatalInternalFault e) {
			log.error("Error occurred processing PurchaseVoucherRequest.", e);
			throw e;
		} catch (ValidationException e) {
			log.error("PurchaseVoucherRequest failed validation.", e);
			throw e;
		}

		log.info(serialize(request.getTxID(), response));
		log.log(SUCCESS, getSuccessMessage(request.getTxID()));
		return response;
	}

	private String serialize(PurchaseVoucherRequest request) {
		return request != null ? "PurchaseVoucherRequest|txID=" + request.getTxID() + 
				"|productID=" + request.getProductID() + 
				"|amount=" + (request.getAmount() != null ? request.getAmount().toPlainString() : "") + 
				"|currency=" + request.getCurrency()
				: null;
	}

	private String serialize(String txID, PurchaseVoucherResponse response) {
		return response != null ? "PurchaseVoucherResponse|txID=" + txID + 
				"|pin=" + getStringUtil().obfuscate(response.getPin()) +
				"|serial=" + getStringUtil().obfuscate(response.getSerial()) + 
				"|valid=" + (response.getValid() != null? response.getValid().toString() : null) 
				: null;
	}

	public GetStateResponse getState(GetStateRequest request)
			throws VoucherNotFoundFault, NotAvailableFault, NotSupportedFault, OtherPostFinanceFault, ValidationException, FatalInternalFault {

		log.info(serialize(request));
		GetStateResponse response = null;

		try {
			getValidationUtil().validate(request);
			response = getGetStateWorkflow().execute(request);

		} catch (VoucherNotFoundFault | NotAvailableFault | OtherPostFinanceFault | FatalInternalFault e) {
			log.error("Error occurred processing GetStateRequest.", e);
			throw e;
		} catch (ValidationException e) {
			log.error("GetStateRequest failed validation.", e);
			throw e;
		}

		log.info(serialize(request.getTxID(), request.getTxRef(), response));
		log.log(SUCCESS, getSuccessMessage(request.getTxID()));
		return response;
	}

	private String serialize(GetStateRequest request) {
		return request != null ? "GetStateRequest|txID=" + request.getTxID() + 
				"|txRef=" + request.getTxRef() + 
				"|serial=" + getStringUtil().obfuscate(request.getSerial()) 
				: null;
	}
	
	private String serialize(String txID, String txRef, GetStateResponse response) {
		return response != null ? "GetStateResponse|txID=" + txID + 
				"|txRef=" + txRef + 
				"|state=" + response.getState()
				: null;
	}

	public CancelVoucherResponse cancel(CancelVoucherRequest request)
			throws VoucherUsedFault, TxRefNotFoundFault, NotAvailableFault, OtherPostFinanceFault, ValidationException, FatalInternalFault {

		log.info(serialize(request));
		CancelVoucherResponse response = null;

		try {
			getValidationUtil().validate(request);
			response = getCancelVoucherWorkflow().execute(request);

		} catch (NotAvailableFault | TxRefNotFoundFault | VoucherUsedFault | OtherPostFinanceFault | FatalInternalFault e) {
			log.error("Error occurred processing CancelVoucherRequest.", e);
			throw e;
		} catch (ValidationException e) {
			log.error("CancelVoucherRequest failed validation.", e);
			throw e;
		}

		log.info(serialize(request.getTxID(), response));
		log.log(SUCCESS, getSuccessMessage(request.getTxID()));
		return response;
	}
	
	private String serialize(CancelVoucherRequest request) {
		return request != null ? "CancelVoucherRequest|txID=" + request.getTxID() +
				"|productID=" + request.getProductID()  +
				"|txRef=" + request.getTxRef() + 
				"|autmatic=" + request.getAutomatic()
				: null;
	}
	
	private String serialize(String txID, CancelVoucherResponse response) {
		return response != null ? "CancelVoucherResponse|txID=" + txID
				: null;
	}

	public PingResponse ping(PingRequest request) {

		log.info("PingRequest|txID=" + request.getTxID());
		PingResponse response = getPingWorkflow().execute(request);
		log.info("PingResponse|txID=" + request.getTxID());
		return response;
	}

	private String getSuccessMessage(String txID) {

		return PP_CODE + "=" + SUCCESS_CODE + "|" + PP_TXID + "=" + txID; 
	}

	public PurchaseVoucherWorkflow getPurchaseVoucherWorkflow() {
		return purchaseVoucherWorkflow;
	}

	public void setPurchaseVoucherWorkflow(
			PurchaseVoucherWorkflow purchaseVoucherWorkflow) {
		this.purchaseVoucherWorkflow = purchaseVoucherWorkflow;
	}

	public GetStateWorkflow getGetStateWorkflow() {
		return getStateWorkflow;
	}

	public void setGetStateWorkflow(GetStateWorkflow getStateWorkflow) {
		this.getStateWorkflow = getStateWorkflow;
	}

	public CancelVoucherWorkflow getCancelVoucherWorkflow() {
		return cancelVoucherWorkflow;
	}

	public void setCancelVoucherWorkflow(CancelVoucherWorkflow cancelVoucherWorkflow) {
		this.cancelVoucherWorkflow = cancelVoucherWorkflow;
	}

	public PingWorkflow getPingWorkflow() {
		return pingWorkflow;
	}

	public void setPingWorkflow(PingWorkflow pingWorkflow) {
		this.pingWorkflow = pingWorkflow;
	}

	public ValidationUtil getValidationUtil() {
		return validationUtil;
	}

	public void setValidationUtil(ValidationUtil validationUtil) {
		this.validationUtil = validationUtil;
	}

	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}

}
