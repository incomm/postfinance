package com.incomm.postfinance.workflow;

import java.util.HashMap;
import java.util.List;

import com.incomm.postfinance.rtg.*;
import com.incomm.postfinance.voucher.message.PurchaseVoucherRequest;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.voucher.fault.FatalInternalFault;
import com.incomm.postfinance.voucher.fault.NotAvailableFault;

@Service
public abstract class AuthenticatedWorkflow extends BaseWorkflow {

	protected static final String EXCEPTION_MAPPING = "exception.mapping";
	private static Logger log = Logger.getLogger(AuthenticatedWorkflow.class);

	protected HashMap<Integer, ExceptionEnum> exceptionMap = new HashMap<Integer, ExceptionEnum>();

	@Autowired AuthProvider authProvider;
	@Autowired RTGDigitalService rtgDigitalService;
	@Autowired RTGUtil rtgUtil;

	public AuthenticatedWorkflow() {

	}

	protected void loadExceptionMapping(String[] keys) throws ConfigurationException {

		for (String key : keys) {

			ExceptionEnum enumValue = null;
			try {
				enumValue = ExceptionEnum.valueOf(key);
			} catch (IllegalArgumentException e) {
				log.warn("Key [" + key + "] is not a valid exception enum value.");
				continue;
			}

			StringBuilder mapping = new StringBuilder(key + " maps from: ");
			String[] values = config.getPropertyAsStringArray(key);
			for (String value : values) {
				try {
					int intValue = Integer.parseInt(value);
					exceptionMap.put(intValue, enumValue);
					mapping.append(intValue + " ");
				} catch (IllegalArgumentException e) {
					log.warn("Failed to parse exception mapping value [" + value + "] for key [" + key + "]");
				}
			}
			log.info(mapping.toString());
		}

	}

	/**
	 * Retrieves a client access token via the AuthProvider
	 * 
	 * @param txID transaction to be authenticated (for logging purposes)
	 * @return authenticated OAuth2 token
	 * @throws NotAvailableFault thrown if the AuthProvider has a problem; NOC Error is logged
	 */
	protected ClientAccessToken authenticate(String txID) throws NotAvailableFault {

		ClientAccessToken token = null;
		try {
			token = getAuthProvider().getClientAccessToken();

		} catch (RTGException e) {

			// Log exception for NOC
			log.fatal(PP_CODE + "=" + e.getRespCode() + "|" + 
					PP_TXID + "=" + txID + "|" + 
					PP_URL + "=" + getAuthProvider().getOAuth2URL());

			// Throw exception to be returned to requester
			throw new NotAvailableFault(NOT_AVAILABLE_MESSAGE, e);
		}
		return token;
	}

	/**
	 * Logs the RTGException's ppRespCode for the NOC and throws an appropriate fault
	 * 
	 * @param e exception to be logged and converted into a fault
	 * @param txID transaction in which the fault occurred (for logging purposes)
	 * @throws NotAvailableFault the transaction is in a known, unprocessed / unfulfilled state
	 * @throws FatalInternalFault the transaction is in an unknown state
	 */
	protected void throwFault(RTGException e, String txID) throws NotAvailableFault, FatalInternalFault {

		// Log exception for NOC
		log.fatal(PP_CODE + "=" + e.getRespCode() + "|" + 
				PP_TXID + "=" + txID + "|" + 
				PP_URL + "=" + getAuthProvider().getOAuth2URL());

		// Throw fault to be sent to requester
		if (e.getRespCode() == RTG_RECEIVE_TIMEOUT_CODE) {
			throw new FatalInternalFault(getConfig().getProperty(RTG_RESPONSE_TIMEOUT_MESSAGE), e);
		} else {
			throw new NotAvailableFault(e);
		}
	}

	/**
	 * Parses an integer from the RTGResponse's responseCode string.
	 * 
	 * @param response contains the response code to parse
	 * @return integer representation of the response code
	 * @throws FatalInternalFault Caused by a NumberFormatException during parsing; NOC Error is logged
	 */
	protected int getResponseCode(RTGResponse response) throws FatalInternalFault {

		try {
			return Integer.parseInt(response.getResponseCode());

		} catch (NumberFormatException e) {
			log.fatal(PP_CODE + "=" + RTG_INVALID_RESPONSE_CODE + "|" + 
					PP_INVALID_RESPONSE + "=" + response.getResponseCode() + "|"+ 
					PP_TXID + "=" + response.getTransactionID());
			throw new FatalInternalFault(getConfig().getProperty(RTG_RESPONSE_CODE_MESSAGE));
		}
	}

	protected ExceptionEnum getException(int code) {

		ExceptionEnum exception = exceptionMap.get(code);
		if (exception == null) 
			exception = ExceptionEnum.Other;
		return exception;
	}

	protected String getResponseMessage(RTGRequest request, RTGResponse response) {

		return "txID=" + request.getTransactionID() + "|rtgRespCode="+ response.getResponseCode() + "|rtgRespText=" + response.getResponseText();
	}

	protected String getNOCMessage(int ppRespCode, String txID) {

		return PP_CODE + "=" + ppRespCode + "|" + PP_TXID + "=" + txID;
	}

	public AuthProvider getAuthProvider() {
		return authProvider;
	}

	public void setAuthProvider(AuthProvider auth) {
		this.authProvider = auth;
	}

	public RTGDigitalService getRtgDigitalService() {
		return rtgDigitalService;
	}

	public void setRtgDigitalService(RTGDigitalService rtgDigitalService) {
		this.rtgDigitalService = rtgDigitalService;
	}

	public RTGUtil getRtgUtil() {
		return rtgUtil;
	}

	public void setRtgUtil(RTGUtil rtgUtil) {
		this.rtgUtil = rtgUtil;
	}

}
