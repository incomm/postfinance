package com.incomm.postfinance.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.config.PostFinanceConfig;
import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.logging.LogConstants;

@Service
public abstract class BaseWorkflow implements LogConstants, PropertyConstants {

	@Autowired PostFinanceConfig config;
	
	public BaseWorkflow() {

	}

	public PostFinanceConfig getConfig() {
		return config;
	}

	public void setConfig(PostFinanceConfig config) {
		this.config = config;
	}

}
