package com.incomm.postfinance.workflow;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.rtg.RTGException;
import com.incomm.postfinance.rtg.RTGRequest;
import com.incomm.postfinance.rtg.RTGResponse;
import com.incomm.postfinance.voucher.fault.FatalInternalFault;
import com.incomm.postfinance.voucher.fault.NotAvailableFault;
import com.incomm.postfinance.voucher.fault.OtherPostFinanceFault;
import com.incomm.postfinance.voucher.fault.TxRefNotFoundFault;
import com.incomm.postfinance.voucher.fault.VoucherUsedFault;
import com.incomm.postfinance.voucher.message.CancelVoucherRequest;
import com.incomm.postfinance.voucher.message.CancelVoucherResponse;

@Service("cancelVoucherWorkflow")
public class CancelVoucherWorkflow extends AuthenticatedWorkflow {

	private static final String[] EXCEPTION_KEYS = {PropertyConstants.TX_REF_NOT_FOUND, PropertyConstants.VOUCHER_USED};
	private static Logger log = Logger.getLogger(CancelVoucherWorkflow.class);

	public CancelVoucherWorkflow() {
		
	}
	
	@PostConstruct
	public void init() throws ConfigurationException {
		loadExceptionMapping(EXCEPTION_KEYS);
	}

	public CancelVoucherResponse execute(CancelVoucherRequest request) 
			throws NotAvailableFault, TxRefNotFoundFault, VoucherUsedFault, OtherPostFinanceFault, FatalInternalFault {

		// Get OAuth2 token
		ClientAccessToken token = authenticate(request.getTxID());

		// Convert voucher request to RTG request
		RTGRequest rtgRequest = translate(request);

		// Send RTG request to RTG Digital Service
		RTGResponse rtgResponse = null;
		try {
			rtgResponse = rtgDigitalService.cancel(rtgRequest, token);
		} catch (RTGException e) {
			throwFault(e, request.getTxID());
		}

		// Convert RTG response into voucher response
		CancelVoucherResponse response = translate(rtgRequest, rtgResponse);

		return response;
	}

	private RTGRequest translate(CancelVoucherRequest request) {

		RTGRequest req = getRtgUtil().getDigitalRequest();

		// Populate request-specific fields
		req.setTransactionID(request.getTxRef());
		
		// Hari says only TxID is required
		//req.getProduct().setUpc(request.getProductID());

		return req;
	}

	private CancelVoucherResponse translate(RTGRequest rtgRequest, RTGResponse response)
			throws NotAvailableFault, TxRefNotFoundFault, VoucherUsedFault, OtherPostFinanceFault, FatalInternalFault {
		
		// Parse response code
		int respCode = getResponseCode(response);
		
		// 0 = success, 1 = thank you
		// Throw exception otherwise
		if (respCode > 1) {
			ExceptionEnum exception = getException(respCode);
			String message = getResponseMessage(rtgRequest, response);
			switch (exception) {
			case TxRefNotFound:
				throw new TxRefNotFoundFault(message);
			case VoucherUsed:
				throw new VoucherUsedFault(message);
			case NotAvailable:
				throw new NotAvailableFault(message);
			default:
				throw new OtherPostFinanceFault(message);
			}
		}
		
		// empty (but happy) response
		return new CancelVoucherResponse();
	}
	
	public void throwResponseCodeException(int responseCode, String responseText)
			throws NotAvailableFault, TxRefNotFoundFault, VoucherUsedFault, OtherPostFinanceFault {
		
		
	}
	
}
