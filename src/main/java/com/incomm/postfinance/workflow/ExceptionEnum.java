package com.incomm.postfinance.workflow;

public enum ExceptionEnum {
	TxRefNotFound,
	VoucherUsed,
	NotAvailable,
	InvalidProduct,
	InvalidAmount,
	VoucherNotFound,
	NotSupported,
	Other
}
