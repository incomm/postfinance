package com.incomm.postfinance.workflow;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.rtg.RTGException;
import com.incomm.postfinance.rtg.RTGRequest;
import com.incomm.postfinance.rtg.RTGResponse;
import com.incomm.postfinance.voucher.fault.FatalInternalFault;
import com.incomm.postfinance.voucher.fault.NotAvailableFault;
import com.incomm.postfinance.voucher.fault.NotSupportedFault;
import com.incomm.postfinance.voucher.fault.OtherPostFinanceFault;
import com.incomm.postfinance.voucher.fault.VoucherNotFoundFault;
import com.incomm.postfinance.voucher.message.GetStateRequest;
import com.incomm.postfinance.voucher.message.GetStateResponse;

@Service("getStateWorkflow")
public class GetStateWorkflow extends AuthenticatedWorkflow {

	public static final int ACTIVE_STATE = 0;
	public static final int USED_STATE = 1;
	public static final int DEACTIVE_STATE = 2;
	public static final int UNKNOWN_STATE = 3;
	
	private static final String[] EXCEPTION_KEYS = {PropertyConstants.VOUCHER_NOT_FOUND, PropertyConstants.NOT_SUPPORTED};
	private static Logger log = Logger.getLogger(GetStateWorkflow.class);

	public GetStateWorkflow() {
		
	}
	
	@PostConstruct
	public void init() throws ConfigurationException {
		loadExceptionMapping(EXCEPTION_KEYS);
	}

	public GetStateResponse execute(GetStateRequest request) 
			throws VoucherNotFoundFault, NotAvailableFault, OtherPostFinanceFault, FatalInternalFault, NotSupportedFault {

		// Get OAuth2 token
		ClientAccessToken token = authenticate(request.getTxID());

		// Convert voucher request to RTG request
		RTGRequest rtgRequest = translate(request);

		// Send RTG request to RTG Digital Service
		RTGResponse rtgResponse = null;
		try {
			rtgResponse = rtgDigitalService.getState(rtgRequest, token);
		} catch (RTGException e) {
			throwFault(e, request.getTxID());
		}

		// Convert RTG response into voucher response
		GetStateResponse response = translate(rtgRequest, rtgResponse);

		return response;
	}

	private RTGRequest translate(GetStateRequest request) {

		RTGRequest req = getRtgUtil().getDigitalRequest();

		// Populate request-specific fields
		req.setTransactionID(request.getTxRef());
		
		// Hari says only TxID is required
		//req.getProduct().setCode(request.getSerial());

		return req;
	}

	private GetStateResponse translate(RTGRequest rtgRequest, RTGResponse response)
			throws VoucherNotFoundFault, NotAvailableFault, OtherPostFinanceFault, NotSupportedFault, FatalInternalFault {
		
		// Parse response code
		int respCode = getResponseCode(response);
		
		// 0 = success, 1 = thank you
		// Throw exception otherwise
		if (respCode > 1) {
			ExceptionEnum exception = getException(respCode);
			String message = getResponseMessage(rtgRequest, response);
			switch (exception) {
			case VoucherNotFound:
				throw new VoucherNotFoundFault(message);
			case NotAvailable:
				throw new NotAvailableFault(message);
			case NotSupported:
				throw new NotSupportedFault(message);
			default:
				throw new OtherPostFinanceFault(message);
			}
		}
		
		// Create happy response
		GetStateResponse resp = new GetStateResponse();
		
		String status = response.getProduct().getStatus();
		double balance = 0;
		try {
			balance = Double.parseDouble(response.getProduct().getBalance());
		} catch (NumberFormatException e) {
		}
		
		if (status.equalsIgnoreCase("A") || status.equalsIgnoreCase("Active")) {
			if (balance > 0) resp.setState(ACTIVE_STATE);
			else resp.setState(USED_STATE);
		}
		else resp.setState(DEACTIVE_STATE);
		
		return resp;
	}
	
	public void throwResponseCodeException(int responseCode, String responseText)
			throws VoucherNotFoundFault, NotAvailableFault, NotSupportedFault, OtherPostFinanceFault {
		
		
	}
}
