package com.incomm.postfinance.workflow;

import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.rtg.RTGException;
import com.incomm.postfinance.rtg.RTGRequest;
import com.incomm.postfinance.voucher.fault.NotAvailableFault;
import com.incomm.postfinance.voucher.message.PingRequest;
import com.incomm.postfinance.voucher.message.PingResponse;

@Service("pingWorkflow")
public class PingWorkflow {

//	private static String NONEXISTENT_TX_ID = "1";
	private static Logger log = Logger.getLogger(PingWorkflow.class);

	public PingWorkflow() {

	}

	public PingResponse execute(PingRequest request) {

		PingResponse response = new PingResponse();
		response.setAvailable(true);

		//		try {
		//			ClientAccessToken token = authenticate(request.getTxID());
		//			RTGRequest rtgRequest = new RTGRequest();
		//			rtgRequest.setOrigin(rtgUtil.getOrigin());
		//			rtgRequest.setTransactionID(NONEXISTENT_TX_ID);
		//			rtgDigitalService.getState(rtgRequest, token);
		//			
		//		} catch (NotAvailableFault | RTGException e) {
		//			response.setAvailable(false);
		//		}

		return response;
	}
}
