package com.incomm.postfinance.workflow;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.rtg.RTGException;
import com.incomm.postfinance.rtg.RTGRequest;
import com.incomm.postfinance.rtg.RTGResponse;
import com.incomm.postfinance.utils.DateUtil;
import com.incomm.postfinance.voucher.fault.FatalInternalFault;
import com.incomm.postfinance.voucher.fault.InvalidAmountFault;
import com.incomm.postfinance.voucher.fault.InvalidProductFault;
import com.incomm.postfinance.voucher.fault.NotAvailableFault;
import com.incomm.postfinance.voucher.fault.OtherPostFinanceFault;
import com.incomm.postfinance.voucher.message.PurchaseVoucherRequest;
import com.incomm.postfinance.voucher.message.PurchaseVoucherResponse;

import java.util.Date;

@Service("purchaseVoucherWorkflow")
public class PurchaseVoucherWorkflow extends AuthenticatedWorkflow {
	
	private static final String[] EXCEPTION_KEYS = {PropertyConstants.INVALID_PRODUCT, PropertyConstants.INVALID_AMOUNT};
	private static Logger log = Logger.getLogger(PurchaseVoucherWorkflow.class);

	public PurchaseVoucherWorkflow() {

	}
	
	@PostConstruct
	public void init() throws ConfigurationException {
		loadExceptionMapping(EXCEPTION_KEYS);
	}
	
	public PurchaseVoucherResponse execute(PurchaseVoucherRequest request) 
			throws NotAvailableFault, InvalidProductFault, InvalidAmountFault, OtherPostFinanceFault, FatalInternalFault {
				
		// Get OAuth2 token
		ClientAccessToken token = authenticate(request.getTxID());
		
		// Convert voucher request to RTG request
		RTGRequest rtgRequest = translate(request);
		
		// Send RTG request to RTG Digital Service
		RTGResponse rtgResponse = null;
		try {
			rtgResponse = getRtgDigitalService().purchase(rtgRequest, token);
		} catch (RTGException e) {
			throwFault(e, request.getTxID());
		}
		
		// Convert RTG response into voucher response
		PurchaseVoucherResponse response = translate(rtgRequest, rtgResponse, request.getProductID());
		
		return response;
	}
	
	private RTGRequest translate(PurchaseVoucherRequest request) {
		
		RTGRequest req = getRtgUtil().getDigitalRequest();
		
		// Populate request-specific fields
		req.setTransactionID(request.getTxID());
		req.getProduct().setUpc(request.getProductID());
		req.getProduct().setAmount(request.getAmount().toPlainString());
		req.getOrigin().setCurrencyCode(request.getCurrency());

		return req;
	}
	
	private PurchaseVoucherResponse translate(RTGRequest rtgRequest, RTGResponse response, String upc)
			throws InvalidProductFault, InvalidAmountFault, NotAvailableFault, OtherPostFinanceFault, FatalInternalFault {
		
		// Parse response code
		int respCode = getResponseCode(response);
		
		// 0 = success, 1 = thank you
		// Throw exception otherwise
		if (respCode > 1) {
			ExceptionEnum exception = getException(respCode);
			String message = getResponseMessage(rtgRequest, response);
			switch (exception) {
			case NotAvailable:
				throw new NotAvailableFault(message);
			case InvalidProduct:
				throw new InvalidProductFault(message);
			case InvalidAmount:
				throw new InvalidAmountFault(message);
			default:
				throw new OtherPostFinanceFault(message);
			}
		}
		
		// Create happy response
		PurchaseVoucherResponse resp = new PurchaseVoucherResponse();
		// postfinance spec says to return the expiration date, which this is not
		// and its not really supported for most products, so were going to leave it null for now
		//resp.setValid(DateUtil.getCurrentZuluTime().toGregorianCalendar().getTime());
		
		String pin = getRtgUtil().getPin(response, upc);
		String serial = getRtgUtil().getSerial(response, upc);
		
		resp.setPin(pin);
		resp.setSerial(serial); 
		
		return resp;
	}
	
}
