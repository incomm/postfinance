# Edit this file to set custom options
# Tomcat accepts two parameters JAVA_OPTS and CATALINA_OPTS
# JAVA_OPTS are used during START/STOP/RUN
# CATALINA_OPTS are used during START/RUN

# DEV, UAT and PROD supported for {env} value
INCOMM_OPTS="-Denv=UAT -Dpostfinance.dir=/var/opt/vmware/vfabric-tc-server/postfinance"

JAVA_HOME="/usr/local/lib64/jdk1.8.0_31"
AGENT_PATHS=""
JAVA_AGENTS=""
JAVA_LIBRARY_PATH=""
JVM_OPTS="-Xmx512M -Xss256K"
JAVA_OPTS="$JVM_OPTS $AGENT_PATHS $JAVA_AGENTS $JAVA_LIBRARY_PATH $INCOMM_OPTS"
