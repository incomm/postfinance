package com.incomm.postfinance.rtg;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.incomm.postfinance.config.PostFinanceConfig;
import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.logging.LogConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationBeans.xml"})
public class AuthProviderTest implements LogConstants {
	
	static Logger log = Logger.getLogger(AuthProviderTest.class);
	@Autowired AuthProvider auth;
	@Autowired PostFinanceConfig config;
	
	public AuthProviderTest() {
		
	}
	
	@BeforeClass
	public static void beforeClass() {
		
	}
	
	@Before
	public void beforeTest() throws ConfigurationException {
	}
	
	@Test
	public void authenticateTest() {
		log.info("authenticateTest");
		
		try {
			authenticate();
		} catch (RTGException e) {
			Assert.fail("Failed to authenticate with valid settings.");
		}
	}
	
	@Test
	public void invalidUrlTest() {
		log.info("invalidUrlTest");
		
		String old = config.getProperty(PropertyConstants.OAUTH2_URL);
		config.setProperty(PropertyConstants.OAUTH2_URL, "http://localhost:1234");
		try {
			authenticate();
			Assert.fail("AuthProvider failed to throw exception.");
		} catch (RTGException e) {
			Assert.assertTrue("AuthProvider exception did not contain proper response code.", e.getRespCode() == OAUTH2_NOT_AVAILABLE_CODE);
		}
		config.setProperty(PropertyConstants.OAUTH2_URL, old);
	}
	
	@Test
	public void invalidCredentialsTest() {
		log.info("invalidCredentialsTest");
		
		String oldID = config.getProperty(PropertyConstants.OAUTH2_CLIENT_ID);
		String oldSecret = config.getProperty(PropertyConstants.OAUTH2_CLIENT_SECRET);
		config.setProperty(PropertyConstants.OAUTH2_CLIENT_ID, "some_sneaky_fella");
		config.setProperty(PropertyConstants.OAUTH2_CLIENT_SECRET, "actually_doctor_evil");
		try {
			authenticate();
			Assert.fail("AuthProvider failed to throw exception.");
		} catch (RTGException e) {
			Assert.assertTrue("AuthProvider exception did not contain proper response code.", e.getRespCode() == OAUTH2_INVALID_CREDENTIALS_CODE);
		}
		config.setProperty(PropertyConstants.OAUTH2_CLIENT_ID, oldID);
		config.setProperty(PropertyConstants.OAUTH2_CLIENT_SECRET, oldSecret);
	}

	private void authenticate() throws RTGException {
		
		try {
			ClientAccessToken token = auth.getClientAccessToken();
			log.info("Token: " + token.getTokenKey());
		} catch (RTGException e) {
			log.info("AuthProviderException during test: " + e.getMessage(), e);
			throw e;
		}
	}
}
