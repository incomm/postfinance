package com.incomm.postfinance.rtg;

import java.util.Calendar;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.incomm.postfinance.config.PostFinanceConfig;
import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.logging.LogConstants;
import com.incomm.postfinance.testsupport.TestSupport;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationBeans.xml"})
public class RTGDigitalServiceTest implements TestSupport, LogConstants {

	static Logger log = Logger.getLogger(RTGDigitalServiceTest.class);

	@Autowired PostFinanceConfig config;
	@Autowired RTGDigitalService rtg;
	@Autowired AuthProvider auth;
	@Autowired RTGUtil rtgUtil;

	static String CANCEL = "Cancel";
	static String GETSTATE = "GetState";
	static String PURCHASE = "Purchase";

	ClientAccessToken token;
	RTGRequest req;
	RTGResponse resp;

	public RTGDigitalServiceTest() {

	}

	@BeforeClass
	public static void beforeClass() {

		
	}

	@Before
	public void beforeTest() throws ConfigurationException {

		req = rtgUtil.getDigitalRequest();
		resp = null;

		req.getProduct().setUpc(UPC);
		req.getProduct().setAmount(BLIZ_AMOUNT.toString());
		req.setTransactionID(Long.toString(Calendar.getInstance().getTimeInMillis()));
	}
	
	@Test
	public void purchaseAndGetStateTest() {

		log.info("purchaseAndGetStateTest");
		try {
			
			authenticate();
			resp = rtg.purchase(req, token);
			Assert.assertTrue("Purchase Response is null", resp != null);
			Assert.assertTrue("Purchase did not indicate success.", resp.getResponseCode().equals("0"));

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Valid purchase configuration threw exception");
		}
		
		try {
			authenticate();
			req.getProduct().setUpc(null);
			resp = rtg.getState(req, token);
			Assert.assertTrue("GetState Response is null", resp != null);
			Assert.assertTrue("GetState did not indicate success.", resp.getResponseCode().equals("0"));
			Assert.assertTrue("GetState did not return an Active Code response.", resp.getProduct().getStatus().equalsIgnoreCase("A"));

		} catch (RTGException e) {
			Assert.fail("Valid getstate configuration threw exception");
		}
	}

	@Test
	public void purchaseExceptionTest() {

		String old = config.getProperty(PropertyConstants.RTG_PURCHASE_URL);
		config.setProperty(PropertyConstants.RTG_PURCHASE_URL, "http://localhost:12324");
		try {
			authenticate();
			resp = rtg.purchase(req, token);
			Assert.fail("Invalid configuration did not throw exception.");

		} catch (RTGException e) {
			log.info("Succesfully faulted: " + e.getMessage(), e);
			Assert.assertTrue("Incorrect error code", e.getRespCode() == RTG_NOT_AVAILABLE_CODE);
		}
		config.setProperty(PropertyConstants.RTG_PURCHASE_URL, old);
	}
	
	@Test
	public void purchaseReceiveExceptionTest() {

		log.info("purchaseReceiveExceptionTest");
		rtg.receiveTimeout = 1L;
		try {
			authenticate();
			resp = rtg.purchase(req, token);
			Assert.fail("Invalid configuration did not throw exception.");

		} catch (RTGException e) {
			log.info("Succesfully faulted: " + e.getMessage(), e);
			Assert.assertTrue("Incorrect error code", e.getRespCode() == RTG_RECEIVE_TIMEOUT_CODE);
		}
		rtg.receiveTimeout = 8000L;
	}

	@Test
	public void cancelTest() {

		log.info("cancelTest");
		try {
			authenticate();
			resp = rtg.cancel(req, token);
			Assert.assertTrue("Cancel Response is null", resp != null);

		} catch (RTGException e) {
			log.info(e);
			Assert.fail("Cancel threw exception.");
		}
	}

	@Test 
	public void cancelExceptionTest() {

		String old = config.getProperty(PropertyConstants.RTG_CANCEL_URL);
		config.setProperty(PropertyConstants.RTG_CANCEL_URL, "http://localhost:12324");
		try {
			authenticate();
			resp = rtg.cancel(req, token);
			Assert.fail("Invalid configuration did not throw exception.");

		} catch (RTGException e) {
			log.info("Succesfully faulted: " + e.getMessage());
			Assert.assertTrue("Incorrect error code", e.getRespCode() == RTG_NOT_AVAILABLE_CODE);
		}
		config.setProperty(PropertyConstants.RTG_CANCEL_URL, old);
	}
	
	@Test 
	public void cancelReceiveTimeoutTest() {

		log.info("cancelReceiveTimeoutTest");
		rtg.receiveTimeout = 1L;
		try {
			authenticate();
			resp = rtg.cancel(req, token);
			Assert.fail("Invalid configuration did not throw exception.");

		} catch (RTGException e) {
			log.info("Succesfully faulted: " + e.getMessage());
			Assert.assertTrue("Incorrect error code", e.getRespCode() == RTG_RECEIVE_TIMEOUT_CODE);
		}
		rtg.receiveTimeout = 8000L;
	}

	@Test 
	public void getStateExceptionTest() {

		String old = config.getProperty(PropertyConstants.RTG_GETSTATE_URL);
		config.setProperty(PropertyConstants.RTG_GETSTATE_URL, "http://localhost:12324");
		try {
			authenticate();
			resp = rtg.getState(req, token);
			Assert.fail("Invalid configuration did not throw exception.");

		} catch (RTGException e) {
			log.info("Succesfully faulted: " + e.getMessage());
			Assert.assertTrue("Incorrect error code", e.getRespCode() == RTG_NOT_AVAILABLE_CODE);
		}
		config.setProperty(PropertyConstants.RTG_GETSTATE_URL, old);
	}
	
	@Test 
	public void getStateReceiveExceptionTest() {

		rtg.receiveTimeout = 1L;
		try {
			authenticate();
			resp = rtg.getState(req, token);
			Assert.fail("Invalid configuration did not throw exception.");

		} catch (RTGException e) {
			log.info("Succesfully faulted: " + e.getMessage());
			Assert.assertTrue("Incorrect error code", e.getRespCode() == RTG_RECEIVE_TIMEOUT_CODE);
		}
		rtg.receiveTimeout = 8000L;
	}

	private void authenticate() {

		try {
			token = auth.getClientAccessToken();

		} catch (RTGException e) {
			Assert.fail("Failed to authenticate.");;
		}
	}

}
