package com.incomm.postfinance.rtg;

import java.util.Calendar;

import javax.xml.bind.JAXBException;

import generated.MetaField;
import generated.ProductResponse;
import generated.ProductResponse.Metafields;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.incomm.postfinance.config.PostFinanceConfig;
import com.incomm.postfinance.config.PropertyConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationBeans.xml"})
public class RTGUtilTest implements PropertyConstants {

	@Autowired RTGUtil util;
	@Autowired PostFinanceConfig config;
	
	String PIN_UPC = "pin1234";
	String SERIAL_UPC = "serial1234";
	String PIN_DEFAULT = "pinDefault";
	String SERIAL_DEFAULT = "serialDefault";
	
	String FAILED_PIN = "Failed to locate PIN.";
	String FAILED_SERIAL = "Failed to locate Serial Number";
	
	static RTGResponse response;
	
	public RTGUtilTest() {
		
	}
	
	@BeforeClass
	public static void beforeClass() {
		
		response = new RTGResponse();
		response.setTransactionID("1");
		response.setProduct(new ProductResponse());
		response.setProduct(new ProductResponse());
		response.getProduct().setMetafields(new Metafields());
	}

	@Test
	public void obfuscationTest() throws JAXBException {
		
		response.getProduct().getMetafields().getMetafield().clear();
		response.getProduct().getMetafields().getMetafield().add(createField("pin", "1111222222221111"));
		
		String marshalled = util.marshal(response);
		System.out.println(marshalled);
		Assert.assertTrue("Middle not obfuscated", !marshalled.contains("22222222"));
		System.out.println("Object value: " + response.getProduct().getMetafields().getMetafield().get(0).getValue());
		Assert.assertTrue("Broke object in the process", 
				response.getProduct().getMetafields().getMetafield().get(0).getValue().equals("1111222222221111"));
	}
	
	private MetaField createField(String name, String value) {
		MetaField field = new MetaField();
		field.setName(name);
		field.setValue(value);
		return field;
	}
	
	@Test
	public void upcMappingTest() {
		
		// Constants
		String upc = Long.toString(Calendar.getInstance().getTimeInMillis());
		String pin = "pinUpcMappingTest";
		String serial = "serialUpcMappingTest";
		
		// Setup Config
		config.setProperty(PIN_KEY + "." + upc, PIN_UPC);
		config.setProperty(SERIAL_KEY + "." + upc, SERIAL_UPC);
		
		// Setup Response Fields
		response.getProduct().getMetafields().getMetafield().clear();
		response.getProduct().getMetafields().getMetafield().add(createField(PIN_UPC, pin));
		response.getProduct().getMetafields().getMetafield().add(createField(SERIAL_UPC, serial));
		
		// Find PIN and Serial
		String foundPin = util.getPin(response, upc);
		String foundSerial = util.getSerial(response, upc);
		
		// Reset Config
		config.setProperty(PIN_KEY + "." + upc, null);
		config.setProperty(SERIAL_KEY + "." + upc, null);
		
		// Assert findings
		Assert.assertTrue(FAILED_PIN, foundPin.equals(pin));
		Assert.assertTrue(FAILED_SERIAL, foundSerial.equals(serial));
	}
	
	@Test
	public void defaultMappingTest() {
		
		// Constants
		String upc = Long.toString(Calendar.getInstance().getTimeInMillis());
		String pin = "pinDefaultMappingTest";
		String serial = "serialDefaultMappingTest";
		
		// Setup Config
		config.setProperty(DEFAULT_PIN_KEY, PIN_DEFAULT);
		config.setProperty(DEFAULT_SERIAL_KEY, SERIAL_DEFAULT);
		
		// Setup Response Fields
		response.getProduct().getMetafields().getMetafield().clear();
		response.getProduct().getMetafields().getMetafield().add(createField(PIN_DEFAULT, pin));
		response.getProduct().getMetafields().getMetafield().add(createField(SERIAL_DEFAULT, serial));
		
		// Find PIN and Serial
		String foundPin = util.getPin(response, upc);
		String foundSerial = util.getSerial(response, upc);
		
		// Reset Config
		config.setProperty(DEFAULT_PIN_KEY, null);
		config.setProperty(DEFAULT_SERIAL_KEY, null);
		
		// Assert findings
		Assert.assertTrue(FAILED_PIN, foundPin.equals(pin));
		Assert.assertTrue(FAILED_SERIAL, foundSerial.equals(serial));
	}
	
	@Test
	public void hardcodedMappingTest() {
		
		// Constants
		String upc = Long.toString(Calendar.getInstance().getTimeInMillis());
		String pin = "pinHardcodedMappingTest";
		String serial = "serialHardcodedMappingTest";
		
		// Ensure no configuration
		config.setProperty(DEFAULT_PIN_KEY, null);
		config.setProperty(DEFAULT_SERIAL_KEY, null);
		config.setProperty(PIN_KEY + "." + upc, null);
		config.setProperty(SERIAL_KEY + "." + upc, null);
		
		// Setup Response Fields
		response.getProduct().getMetafields().getMetafield().clear();
		response.getProduct().getMetafields().getMetafield().add(createField(RTGUtil.HARDCODED_DEFAULT_PIN_KEY, pin));
		response.getProduct().getMetafields().getMetafield().add(createField(RTGUtil.HARDCODED_DEFAULT_SERIAL_KEY, serial));
		
		// Find PIN and Serial
		String foundPin = util.getPin(response, upc);
		String foundSerial = util.getSerial(response, upc);

		// Assert findings
		Assert.assertTrue(FAILED_PIN, foundPin.equals(pin));
		Assert.assertTrue(FAILED_SERIAL, foundSerial.equals(serial));
	}
}
