package com.incomm.postfinance.service;

import java.math.BigDecimal;

import javax.validation.ValidationException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.incomm.postfinance.testsupport.TestSupport;
import com.incomm.postfinance.utils.DateUtil;
import com.incomm.postfinance.utils.StringUtil;
import com.incomm.postfinance.utils.ValidationUtil;
import com.incomm.postfinance.voucher.fault.FatalInternalFault;
import com.incomm.postfinance.voucher.fault.InvalidAmountFault;
import com.incomm.postfinance.voucher.fault.InvalidProductFault;
import com.incomm.postfinance.voucher.fault.NotAvailableFault;
import com.incomm.postfinance.voucher.fault.NotSupportedFault;
import com.incomm.postfinance.voucher.fault.OtherPostFinanceFault;
import com.incomm.postfinance.voucher.fault.TxRefNotFoundFault;
import com.incomm.postfinance.voucher.fault.VoucherNotFoundFault;
import com.incomm.postfinance.voucher.fault.VoucherUsedFault;
import com.incomm.postfinance.voucher.message.CancelVoucherRequest;
import com.incomm.postfinance.voucher.message.CancelVoucherResponse;
import com.incomm.postfinance.voucher.message.GetStateRequest;
import com.incomm.postfinance.voucher.message.GetStateResponse;
import com.incomm.postfinance.voucher.message.PingRequest;
import com.incomm.postfinance.voucher.message.PingResponse;
import com.incomm.postfinance.voucher.message.PurchaseVoucherRequest;
import com.incomm.postfinance.voucher.message.PurchaseVoucherResponse;
import com.incomm.postfinance.voucher.service.PostFinanceImpl;
import com.incomm.postfinance.workflow.CancelVoucherWorkflow;
import com.incomm.postfinance.workflow.GetStateWorkflow;
import com.incomm.postfinance.workflow.PingWorkflow;
import com.incomm.postfinance.workflow.PurchaseVoucherWorkflow;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationBeans.xml"})
public class PostFinanceImplTest implements TestSupport {
	
	static Logger log = Logger.getLogger(PostFinanceImplTest.class);

	static String WORKFLOW_FAILURE = "Failed to return expected response from workflow.";
	static String VALIDATION_TEST_FAILURE = "Failed to throw exception for invalid request."; 
	static String EXCEPTION_TEST_FAILURE = "Failed to throw expected exception.";
	static String MOCK_SETUP_FAILURE = "Failed to setup mock object.";

	@InjectMocks PostFinanceImpl postFinance;
	@Mock PurchaseVoucherWorkflow purchaseVoucherWorkflow;
	@Mock CancelVoucherWorkflow cancelVoucherWorkflow;
	@Mock GetStateWorkflow getStateWorkflow;
	@Mock PingWorkflow pingWorkflow;
	@Spy ValidationUtil validationUtil;
	@Spy StringUtil stringUtil;

	PurchaseVoucherRequest purchaseRequest = new PurchaseVoucherRequest();
	CancelVoucherRequest cancelRequest = new CancelVoucherRequest();
	GetStateRequest getStateRequest = new GetStateRequest();
	PingRequest pingRequest = new PingRequest();

	PurchaseVoucherResponse purchaseResponse = new PurchaseVoucherResponse();
	CancelVoucherResponse cancelResponse = new CancelVoucherResponse();
	GetStateResponse getStateResponse = new GetStateResponse();
	PingResponse pingResponse = new PingResponse();

	InvalidProductFault invalidProduct = new InvalidProductFault();
	InvalidAmountFault invalidAmount = new InvalidAmountFault();
	NotAvailableFault notAvailable = new NotAvailableFault();
	OtherPostFinanceFault other = new OtherPostFinanceFault();
	TxRefNotFoundFault txRefNotFound = new TxRefNotFoundFault();
	VoucherNotFoundFault voucherNotFound = new VoucherNotFoundFault();
	VoucherUsedFault voucherUsed = new VoucherUsedFault();
	
	public PostFinanceImplTest() {

	}

	@Before
	public void beforeTest() {

		MockitoAnnotations.initMocks(this);

		purchaseRequest.setAmount(BigDecimal.TEN);
		purchaseRequest.setCurrency("USD");
		purchaseRequest.setProductID(UPC);
		purchaseRequest.setAmount(BLIZ_AMOUNT);
		purchaseRequest.setTxID(TxID);
		
		purchaseResponse.setValid(DateUtil.getCurrentZuluTime().toGregorianCalendar().getTime());
		purchaseResponse.setSerial("1234564447890");
		purchaseResponse.setPin("123245678");

		cancelRequest.setAutomatic(Boolean.TRUE);
		cancelRequest.setProductID(UPC);
		cancelRequest.setTxRef(TxID);
		cancelRequest.setTxID(L26);

		getStateRequest.setTxRef(TxID);
		getStateRequest.setTxID(L26);

		pingRequest.setTxID(L26);
	}

	@Test
	public void testConfiguration() {
		Assert.assertNotNull(postFinance);
		Assert.assertNotNull(postFinance.getCancelVoucherWorkflow());
		Assert.assertNotNull(postFinance.getGetStateWorkflow());
		Assert.assertNotNull(postFinance.getPingWorkflow());
		Assert.assertNotNull(postFinance.getPurchaseVoucherWorkflow());
		Assert.assertTrue(postFinance.getCancelVoucherWorkflow() == cancelVoucherWorkflow);
		Assert.assertTrue(postFinance.getGetStateWorkflow() == getStateWorkflow);
		Assert.assertTrue(postFinance.getPingWorkflow() == pingWorkflow);
		Assert.assertTrue(postFinance.getPurchaseVoucherWorkflow() == purchaseVoucherWorkflow);
		Assert.assertTrue(postFinance.getStringUtil() != null);
	}
	
	@Test public void purchaseThrowsInvalidProductTest() { purchaseThrowsTest(invalidProduct); }
	@Test public void purchaseThrowsNotAvailableTest() { purchaseThrowsTest(notAvailable); }
	@Test public void purchaseThrowsInvalidAmountTest() { purchaseThrowsTest(invalidAmount); }
	@Test public void purchaseThrowsOtherFaultTest() { purchaseThrowsTest(other); }
	
	@Test public void cancelThrowsNotAvailableTest() { cancelThrowsTest(notAvailable); }
	@Test public void cancelThrowsOtherFaultTest() { cancelThrowsTest(other); }
	@Test public void cancelThrowsVoucherUsedTest() { cancelThrowsTest(voucherUsed); }
	@Test public void cancelThrowsTxRefNotFoundTest() { cancelThrowsTest(txRefNotFound); }
	
	@Test public void getStateThrowsNotAvailableTest() { getStateThrowsTest(notAvailable); }
	@Test public void getStateThrowsOtherFaultTest() { getStateThrowsTest(other); }
	@Test public void getStateThrowsVoucherNotFoundTest() { getStateThrowsTest(voucherNotFound); }

	@Test
	public void purchaseFlowTest() throws NotAvailableFault, InvalidProductFault, InvalidAmountFault, OtherPostFinanceFault, FatalInternalFault {

		Mockito.doReturn(purchaseResponse).when(purchaseVoucherWorkflow).execute(purchaseRequest);
		Assert.assertTrue(WORKFLOW_FAILURE, postFinance.purchase(purchaseRequest).equals(purchaseResponse));
	}

	@Test
	public void cancelFlowTest() throws NotAvailableFault, TxRefNotFoundFault, VoucherUsedFault, OtherPostFinanceFault, ValidationException, InvalidProductFault, InvalidAmountFault, FatalInternalFault {

		Mockito.doReturn(cancelResponse).when(cancelVoucherWorkflow).execute(cancelRequest);
		Assert.assertTrue(WORKFLOW_FAILURE, postFinance.cancel(cancelRequest).equals(cancelResponse));
	}

	@Test
	public void getStateFlowTest() throws VoucherNotFoundFault, NotAvailableFault, OtherPostFinanceFault, FatalInternalFault, NotSupportedFault {

		Mockito.doReturn(getStateResponse).when(getStateWorkflow).execute(getStateRequest);
		Assert.assertTrue(WORKFLOW_FAILURE, postFinance.getState(getStateRequest).equals(getStateResponse));
	}

	@Test
	public void pingFlowTest() throws NotAvailableFault, InvalidProductFault, InvalidAmountFault, OtherPostFinanceFault {

		Mockito.doReturn(pingResponse).when(pingWorkflow).execute(pingRequest);
		Assert.assertTrue(WORKFLOW_FAILURE, postFinance.ping(pingRequest).equals(pingResponse));
	}

	@Test
	public void purchaseValidationTest() throws InvalidProductFault, InvalidAmountFault, NotAvailableFault, OtherPostFinanceFault, FatalInternalFault {

		try {
			purchaseRequest.setTxID(INVALID_L41);
			postFinance.purchase(purchaseRequest);
			Assert.fail(VALIDATION_TEST_FAILURE);
		} catch (ValidationException e) {
			// Pass
		}
	}

	@Test
	public void cancelValidationTest() throws VoucherUsedFault, TxRefNotFoundFault, NotAvailableFault, OtherPostFinanceFault, FatalInternalFault {

		try {
			cancelRequest.setTxID(INVALID_L41);
			postFinance.cancel(cancelRequest);
			Assert.fail(VALIDATION_TEST_FAILURE);
		} catch (ValidationException e) {
			// Pass
		}
	}

	@Test
	public void getStateValidationTest() throws VoucherNotFoundFault, NotAvailableFault, OtherPostFinanceFault, NotSupportedFault, FatalInternalFault {

		try {
			getStateRequest.setTxID(INVALID_L41);
			postFinance.getState(getStateRequest);
			Assert.fail(VALIDATION_TEST_FAILURE);
		} catch (ValidationException e) {
			// Pass
		}
	}

	@Test
	public void pingValidationTest() throws InvalidProductFault, InvalidAmountFault, NotAvailableFault, OtherPostFinanceFault {

		try {
			pingRequest.setTxID(INVALID_L41);
			postFinance.ping(pingRequest);
			// Pass
		} catch (ValidationException e) {
			// Ping doesn't validate - just pass
			Assert.fail(VALIDATION_TEST_FAILURE);
		}
	}
	
	private void purchaseThrowsTest(Throwable t) {

		try {
			Mockito.doThrow(t).when(purchaseVoucherWorkflow).execute(purchaseRequest);
		} catch (Exception e) {
			log.error(MOCK_SETUP_FAILURE, e);
			Assert.fail(MOCK_SETUP_FAILURE);
		}
		try {
			postFinance.purchase(purchaseRequest);
			Assert.fail(EXCEPTION_TEST_FAILURE);
		} catch (Exception e) {
			Assert.assertTrue(EXCEPTION_TEST_FAILURE, e.getClass().equals(t.getClass()));
		}
	}

	private void cancelThrowsTest(Throwable t) {

		try {
			Mockito.doThrow(t).when(cancelVoucherWorkflow).execute(cancelRequest);
		} catch (Exception e) {
			log.error(MOCK_SETUP_FAILURE, e);
			Assert.fail(MOCK_SETUP_FAILURE);
		}
		try {
			postFinance.cancel(cancelRequest);
			Assert.fail(EXCEPTION_TEST_FAILURE);
		} catch (Exception e) {
			Assert.assertTrue(EXCEPTION_TEST_FAILURE, e.getClass().equals(t.getClass()));
		}
	}

	private void getStateThrowsTest(Throwable t) {

		try {
			Mockito.doThrow(t).when(getStateWorkflow).execute(getStateRequest);
		} catch (Exception e) {
			log.error(MOCK_SETUP_FAILURE, e);
			Assert.fail(MOCK_SETUP_FAILURE);
		}
		try {
			postFinance.getState(getStateRequest);
			Assert.fail(EXCEPTION_TEST_FAILURE);
		} catch (Exception e) {
			Assert.assertTrue(EXCEPTION_TEST_FAILURE, e.getClass().equals(t.getClass()));
		}
	}

}
