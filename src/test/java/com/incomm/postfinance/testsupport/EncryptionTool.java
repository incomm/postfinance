package com.incomm.postfinance.testsupport;

import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.incomm.postfinance.config.PostFinanceConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationBeans.xml"})
public class EncryptionTool {

	static Logger log = Logger.getLogger(EncryptionTool.class);
	
	@Autowired StandardPBEStringEncryptor configurationEncryptor;
	@Autowired PostFinanceConfig config;
	
	public EncryptionTool() {
		
	}

	@Test
	public void jasyptTest() {
		
		log.info("@Encryption");
		
		// WARNING: Do not store sensitive information here after use.
		String user = "oauth2_username";
		String pass = "oauth2_password";

		// don't know why this is here,  its not done in reverse on the actual call, jsut the decrypt from the configurationEncryptor
		// so skip this, if you have a new secret
		String securePassword = config.hash(pass);
		
		String euser = configurationEncryptor.encrypt(user);
		String epass = configurationEncryptor.encrypt(securePassword);
		System.out.println("here is the secret");
		System.out.println(euser);
		System.out.println(epass);
	}

}
