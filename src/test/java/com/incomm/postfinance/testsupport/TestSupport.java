package com.incomm.postfinance.testsupport;

import java.math.BigDecimal;

public interface TestSupport {

	public static String TxID = "4224574578676";
//	public static String UPC = "799366262138";
	public static String UPC = "799366355465";
	
	// Blizzard Cards
	public static BigDecimal BLIZ_AMOUNT = new BigDecimal(20.00);
	public static String BLIZ_CURRENCY = "CHF";
	public static String BLIZ_UPC1 = "799366160953";
	public static String BLIZ_UPC2 = "799366345619";
	public static String BLIZ_UPC3 = "799366345640";
	
	static final String VALID = "VAlId123abcABC";
	static final String L0 = "";
	static final String L10 = "0123456789";
	static final String L26 = "abcdefghijklmnopqrstuvwxyz";
	static final String L31 = L26 + "12345";
	static final String L41 = L31 + L10;
	static final String INVALID_L2 = "!@";
	static final String INVALID_L12 = L10 + INVALID_L2;
	static final String INVALID_L31 = L26 + INVALID_L2 + "12324";
	static final String INVALID_L41 = INVALID_L31 + L10;

}
