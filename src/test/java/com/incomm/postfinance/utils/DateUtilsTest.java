package com.incomm.postfinance.utils;

import java.util.Calendar;

import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Assert;
import org.junit.Test;

public class DateUtilsTest {

	public DateUtilsTest() {

	}
	
	@Test
	public void formatTest() {
		
		Calendar cal = Calendar.getInstance();
		System.out.println(DateUtil.format(cal));
	}
	
	@Test 
	public void currentTimeTest() {
		
		Calendar calBefore = Calendar.getInstance();
		XMLGregorianCalendar greg = DateUtil.getCurrentZuluTime();
		Calendar calAfter = Calendar.getInstance();
		
		Assert.assertTrue("DateUtils.getCurrentZuluTime() incorrect.", 
				calBefore.compareTo(greg.toGregorianCalendar()) <= 0 
				&& calAfter.compareTo(greg.toGregorianCalendar()) >= 0);
	}

}
