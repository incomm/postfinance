package com.incomm.postfinance.utils;

import java.math.BigDecimal;

import javax.validation.ValidationException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.incomm.postfinance.config.ValidationErrors;
import com.incomm.postfinance.testsupport.TestSupport;
import com.incomm.postfinance.voucher.message.CancelVoucherRequest;
import com.incomm.postfinance.voucher.message.GetStateRequest;
import com.incomm.postfinance.voucher.message.PurchaseVoucherRequest;

public class ValidationTest implements TestSupport {
	
	static Logger log;
	static PurchaseVoucherRequest pvr;
	static CancelVoucherRequest cvr;
	static GetStateRequest gsr;
	ValidationException exception;
	
	ValidationUtil validationUtil = new ValidationUtil();

	public ValidationTest() {
		
	}
	
	@BeforeClass
	public static void setup() {

		log = Logger.getLogger(ValidationTest.class);
		pvr = new PurchaseVoucherRequest();
		cvr = new CancelVoucherRequest();
		gsr = new GetStateRequest();
	}
	
	@Before
	public void beforeTest() {
		
		// Reset to valid values
		pvr.setAmount(BigDecimal.valueOf(10.0));
		pvr.setCurrency("USD");
		pvr.setProductID(VALID);
		pvr.setTxID(VALID);
		
		cvr.setAutomatic(true);
		cvr.setProductID(VALID);
		cvr.setTxID(VALID);
		cvr.setTxRef(VALID);
		
		gsr.setTxID(VALID);
		gsr.setTxRef(VALID);
		gsr.setSerial(VALID);
		
		exception = null;
	}
	
	@Test
	public void pvr_ValidTest() {
		
		validTest(pvr);
	}
	
	@Test
	public void cvr_ValidTest() {
		
		validTest(cvr);
	}
	
	@Test
	public void gsr_ValidTest() {
		
		validTest(gsr);
	}
	
	private <T> void validTest(T object) {
		
		exception = validateAndCatch(object);
		Assert.assertTrue("Validation exception for valid object.", exception == null);
	}
	
	@Test
	public void pvr_TxID_NotNullTest() {
		
		pvr.setTxID(null);
		notNullTest(pvr, ValidationErrors.TX_ID_NOT_NULL);
	}
	
	@Test
	public void pvr_ProductID_NotNullTest() {
		
		pvr.setProductID(null);
		notNullTest(pvr, ValidationErrors.PRODUCT_ID_NOT_NULL);
	}
	
	@Test
	public void pvr_Amount_NotNullTest() {
		
		pvr.setAmount(null);
		notNullTest(pvr, ValidationErrors.AMOUNT_NOT_NULL);
	}
	
	@Test
	public void pvr_Currency_NotNullTest() {
		
		pvr.setCurrency(null);
		notNullTest(pvr, ValidationErrors.CURRENCY_NOT_NULL);
	}
	
	@Test
	public void cvr_TxID_NotNullTest() {
		
		cvr.setTxID(null);
		notNullTest(cvr, ValidationErrors.TX_ID_NOT_NULL);
	}
	
//	@Test
//	public void cvr_ProductID_NotNullTest() {
//		
//		cvr.setProductID(null);
//		notNullTest(cvr, ValidationErrors.PRODUCT_ID_NOT_NULL);
//	}
	
	@Test
	public void cvr_TxRef_NotNullTest() {
		
		cvr.setTxRef(null);
		notNullTest(cvr, ValidationErrors.TX_ID_NOT_NULL);
	}
	
	@Test
	public void gsr_TxID_NotNullTest() {
		
		gsr.setTxID(null);
		notNullTest(gsr, ValidationErrors.TX_ID_NOT_NULL);
	}
	
//	@Test
//	public void gsr_Serial_NotNullTest() {
//		
//		gsr.setSerial(null);
//		notNullTest(gsr, ValidationErrors.SERIAL_NOT_NULL);
//	}
	
	@Test
	public void gsr_TxRef_NotNullTest() {
		
		gsr.setTxRef(null);
		notNullTest(gsr, ValidationErrors.TX_ID_NOT_NULL);
	}
	
	public <T> void notNullTest(T object, String error) {
		
		exception = validateAndCatch(object);
		Assert.assertTrue("No validation exception for null value.", exception != null);
		Assert.assertTrue("No validation error message for null value.", 
				exception.getMessage().contains(error));
	}
	
	@Test
	public void pvr_TxID_PatternTest() {
		
		pvr.setTxID(INVALID_L12);
		patternTest(pvr, ValidationErrors.TX_ID_PATTERN);
	}
	
	@Test
	public void pvr_ProductID_PatternTest() {
		
		pvr.setProductID(INVALID_L12);
		patternTest(pvr, ValidationErrors.PRODUCT_ID_PATTERN);
	}
	
	@Test
	public void pvr_Currency_PatternTest() {
		
		pvr.setCurrency(INVALID_L2);
		patternTest(pvr, ValidationErrors.CURRENCY_PATTERN);
	}
	
	@Test
	public void cvr_TxID_PatternTest() {
		
		cvr.setTxID(INVALID_L12);
		patternTest(cvr, ValidationErrors.TX_ID_PATTERN);
	}
	
//	@Test
//	public void cvr_ProductID_PatternTest() {
//		
//		cvr.setProductID(INVALID_L12);
//		patternTest(cvr, ValidationErrors.PRODUCT_ID_PATTERN);
//	}
	
	@Test
	public void cvr_TxRef_PatternTest() {
		
		cvr.setTxRef(INVALID_L12);
		patternTest(cvr, ValidationErrors.TX_ID_PATTERN);
	}
	
	@Test
	public void gsr_TxID_PatternTest() {
		
		gsr.setTxID(INVALID_L12);
		patternTest(gsr, ValidationErrors.TX_ID_PATTERN);
	}
	
//	@Test
//	public void gsr_Serial_PatternTest() {
//		
//		gsr.setSerial(INVALID_L12);
//		patternTest(gsr, ValidationErrors.SERIAL_PATTERN);
//	}
	
	@Test
	public void gsr_TxRef_PatternTest() {
		
		gsr.setTxRef(INVALID_L12);
		patternTest(gsr, ValidationErrors.TX_ID_PATTERN);
	}
	
	public <T> void patternTest(T object, String error) {
		
		exception = validateAndCatch(object);
		Assert.assertTrue("No validation exception for invalid characters.", exception != null);
		Assert.assertTrue("No validation error message for invalid characters.", 
				exception.getMessage().contains(error));
	}
	
	@Test
	public void pvr_TxID_SizeTest() {
		
		pvr.setTxID(L0);
		sizeTest(pvr, ValidationErrors.TX_ID_SIZE);

		pvr.setTxID(L41);
		sizeTest(pvr, ValidationErrors.TX_ID_SIZE);
	}
	
	@Test
	public void pvr_ProductID_SizeTest() {
		
		pvr.setProductID(L0);
		sizeTest(pvr, ValidationErrors.PRODUCT_ID_SIZE);

		pvr.setProductID(L41);
		sizeTest(pvr, ValidationErrors.PRODUCT_ID_SIZE);
	}
	
	@Test
	public void pvr_TxCurrency_SizeTest() {
		
		pvr.setCurrency(L0);
		sizeTest(pvr, ValidationErrors.CURRENCY_SIZE);

		pvr.setCurrency(L41);
		sizeTest(pvr, ValidationErrors.CURRENCY_SIZE);
	}
	
	@Test
	public void cvr_TxID_SizeTest() {
		
		cvr.setTxID(L0);
		sizeTest(cvr, ValidationErrors.TX_ID_SIZE);

		cvr.setTxID(L41);
		sizeTest(cvr, ValidationErrors.TX_ID_SIZE);
	}
	
//	@Test
//	public void cvr_ProductID_SizeTest() {
//		
//		cvr.setProductID(L0);
//		sizeTest(cvr, ValidationErrors.PRODUCT_ID_SIZE);
//
//		cvr.setProductID(L41);
//		sizeTest(cvr, ValidationErrors.PRODUCT_ID_SIZE);
//	}
	
	@Test
	public void cvr_TxRef_SizeTest() {
		
		cvr.setTxRef(L0);
		sizeTest(cvr, ValidationErrors.TX_ID_SIZE);

		cvr.setTxRef(L41);
		sizeTest(cvr, ValidationErrors.TX_ID_SIZE);
	}
	
	@Test
	public void gsr_TxID_SizeTest() {
		
		gsr.setTxID(L0);
		sizeTest(gsr, ValidationErrors.TX_ID_SIZE);

		gsr.setTxID(L41);
		sizeTest(gsr, ValidationErrors.TX_ID_SIZE);
	}
	
//	@Test
//	public void gsr_Serial_SizeTest() {
//		
//		gsr.setSerial(L0);
//		sizeTest(gsr, ValidationErrors.SERIAL_SIZE);
//
//		gsr.setSerial(L31);
//		sizeTest(gsr, ValidationErrors.SERIAL_SIZE);
//	}
	
	@Test
	public void gsr_TxRef_SizeTest() {
		
		gsr.setTxRef(L0);
		sizeTest(gsr, ValidationErrors.TX_ID_SIZE);

		gsr.setTxRef(L41);
		sizeTest(gsr, ValidationErrors.TX_ID_SIZE);
	}
	
	public <T> void sizeTest(T object, String error) {
		
		exception = validateAndCatch(object);
		Assert.assertTrue("No validation exception for inappropriately sized value.", exception != null);
		Assert.assertTrue("No validation error message for inappropriately sized value.", 
				exception.getMessage().contains(error));
	}
	
	@Test
	public void pvr_TxID_SizeAndPatternTest() {

		pvr.setTxID(INVALID_L41);
		sizeAndPatternTest(pvr, ValidationErrors.TX_ID_SIZE, ValidationErrors.TX_ID_PATTERN);
	}
	
	@Test
	public void pvr_ProductID_SizeAndPatternTest() {

		pvr.setProductID(INVALID_L41);
		sizeAndPatternTest(pvr, ValidationErrors.PRODUCT_ID_SIZE, ValidationErrors.PRODUCT_ID_PATTERN);
	}
	
	@Test
	public void pvr_Currency_SizeAndPatternTest() {

		pvr.setCurrency(INVALID_L41);
		sizeAndPatternTest(pvr, ValidationErrors.CURRENCY_SIZE, ValidationErrors.CURRENCY_PATTERN);
		
		pvr.setCurrency("!2");
		sizeAndPatternTest(pvr, ValidationErrors.CURRENCY_SIZE, ValidationErrors.CURRENCY_PATTERN);
	}
	
	@Test
	public void cvr_TxID_SizeAndPatternTest() {

		cvr.setTxID(INVALID_L41);
		sizeAndPatternTest(cvr, ValidationErrors.TX_ID_SIZE, ValidationErrors.TX_ID_PATTERN);
	}
	
//	@Test
//	public void cvr_ProductID_SizeAndPatternTest() {
//
//		cvr.setProductID(INVALID_L41);
//		sizeAndPatternTest(cvr, ValidationErrors.PRODUCT_ID_SIZE, ValidationErrors.PRODUCT_ID_PATTERN);
//	}
	
	@Test
	public void cvr_TxRef_SizeAndPatternTest() {

		cvr.setTxRef(INVALID_L41);
		sizeAndPatternTest(cvr, ValidationErrors.TX_ID_SIZE, ValidationErrors.TX_ID_PATTERN);
	}
	
	@Test
	public void gsr_TxID_SizeAndPatternTest() {

		gsr.setTxID(INVALID_L41);
		sizeAndPatternTest(gsr, ValidationErrors.TX_ID_SIZE, ValidationErrors.TX_ID_PATTERN);
	}
	
//	@Test
//	public void gsr_Serial_SizeAndPatternTest() {
//
//		gsr.setSerial(INVALID_L31);
//		sizeAndPatternTest(gsr, ValidationErrors.SERIAL_SIZE, ValidationErrors.SERIAL_PATTERN);
//	}
	
	@Test
	public void gsr_TxRef_SizeAndPatternTest() {

		gsr.setTxRef(INVALID_L41);
		sizeAndPatternTest(gsr, ValidationErrors.TX_ID_SIZE, ValidationErrors.TX_ID_PATTERN);
	}
	
	public <T> void sizeAndPatternTest(T object, String sizeError, String patternError) {
		
		exception = validateAndCatch(object);
		Assert.assertTrue("No validation exception for inappropriately sized value.", exception != null);
		Assert.assertTrue("No validation error message for inappropriately sized value.", 
				exception.getMessage().contains(sizeError));
		Assert.assertTrue("No validation error message for invalid characters.", 
				exception.getMessage().contains(patternError));
	}
	
	private <T> ValidationException validateAndCatch(T object) {
		
		try {
			validationUtil.validate(object);
		} catch (ValidationException e) {
			log.info("Validation Exception Message: " + e.getMessage());
			return e;
		}
		return null;
	}

}
