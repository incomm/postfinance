package com.incomm.postfinance.workflow;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.incomm.postfinance.voucher.message.PingRequest;
import com.incomm.postfinance.voucher.message.PingResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationBeans.xml"})
public class PingWorkflowTest {
	
	@Autowired PingWorkflow pingWorkflow;
	
	PingRequest pingRequest = new PingRequest();
	

	public PingWorkflowTest() {
		
	}
	
	@Before
	public void beforeTest() throws ConfigurationException {
		
		pingRequest.setTxID("1");
	}
	
	@Test
	public void pingTest() {
		
		PingResponse response = pingWorkflow.execute(pingRequest);
		Assert.assertTrue("Failed to ping valid configuration", response.getAvailable());
	}
	
//	@Test
//	public void authDownTest() {
//		
//		pingWorkflow.getConfig().setProperty(PropertyConstants.OAUTH2_URL, "http://localhost:13241");
//		PingResponse response = pingWorkflow.execute(pingRequest);
//		Assert.assertTrue("Failed to ping valid configuration", !response.getAvailable());
//	}
//	
//	@Test
//	public void rtgDownTest() {
//		
//		pingWorkflow.getConfig().setProperty(PropertyConstants.RTG_GETSTATE_URL, "http://localhost:13241");
//		PingResponse response = pingWorkflow.execute(pingRequest);
//		Assert.assertTrue("Failed to ping valid configuration", !response.getAvailable());
//	}

}
