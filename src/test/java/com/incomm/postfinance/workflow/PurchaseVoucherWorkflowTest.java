package com.incomm.postfinance.workflow;

import generated.MetaField;
import generated.ProductResponse;
import generated.ProductResponse.Metafields;

import java.math.BigDecimal;

import org.apache.cxf.rs.security.oauth2.common.ClientAccessToken;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.incomm.postfinance.config.PostFinanceConfig;
import com.incomm.postfinance.config.PropertyConstants;
import com.incomm.postfinance.rtg.AuthProvider;
import com.incomm.postfinance.rtg.RTGDigitalService;
import com.incomm.postfinance.rtg.RTGException;
import com.incomm.postfinance.rtg.RTGRequest;
import com.incomm.postfinance.rtg.RTGResponse;
import com.incomm.postfinance.rtg.RTGUtil;
import com.incomm.postfinance.testsupport.TestSupport;
import com.incomm.postfinance.voucher.fault.FatalInternalFault;
import com.incomm.postfinance.voucher.fault.InvalidAmountFault;
import com.incomm.postfinance.voucher.fault.InvalidProductFault;
import com.incomm.postfinance.voucher.fault.NotAvailableFault;
import com.incomm.postfinance.voucher.fault.OtherPostFinanceFault;
import com.incomm.postfinance.voucher.message.PurchaseVoucherRequest;
import com.incomm.postfinance.voucher.message.PurchaseVoucherResponse;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationBeans.xml"})
public class PurchaseVoucherWorkflowTest implements TestSupport {
	
	// TODO: Add exception handling tests
	
	static final String PIN_VALUE = "1234567890";
	static final String SERIAL_VALUE = "0987654321";

	@InjectMocks PurchaseVoucherWorkflow purchaseVoucherWorkflow;
	@Mock AuthProvider authProvider;
	@Mock RTGDigitalService rtgDigitalService;
	@Spy RTGUtil rtgUtil;
	@Spy PostFinanceConfig config;
	
	PurchaseVoucherRequest purchaseRequest = new PurchaseVoucherRequest();
	RTGResponse rtgResponse = new RTGResponse(); 
	ClientAccessToken token = new ClientAccessToken("", "");
	
	public PurchaseVoucherWorkflowTest() {
		
	}
	
	@Before
	public void beforeTest() {
		
		MockitoAnnotations.initMocks(this);
		rtgUtil.setConfig(config);
		
		purchaseRequest.setAmount(BigDecimal.TEN);
		purchaseRequest.setCurrency("USD");
		purchaseRequest.setProductID(UPC);
		purchaseRequest.setAmount(BLIZ_AMOUNT);
		purchaseRequest.setTxID(TxID);
		
		rtgResponse.setResponseCode("0");
		rtgResponse.setResponseText("Success");
		rtgResponse.setProduct(new ProductResponse());
		rtgResponse.getProduct().setBalance(purchaseRequest.getAmount().toPlainString());
		rtgResponse.getProduct().setUpc(UPC);
		rtgResponse.getProduct().setAmount(BLIZ_AMOUNT.toString());
		rtgResponse.getProduct().setMetafields(new Metafields());
		rtgResponse.getProduct().getMetafields().getMetafield().add(createField(config.getProperty(PropertyConstants.PIN_KEY + ".default"), PIN_VALUE));
		rtgResponse.getProduct().getMetafields().getMetafield().add(createField(config.getProperty(PropertyConstants.SERIAL_KEY + ".default"), SERIAL_VALUE));
	}
	
	private MetaField createField(String key, String value) {
		MetaField field = new MetaField();
		field.setName(key);
		field.setValue(value);
		return field;
	}
	
	@Test
	public void purchaseWorkflowTest() throws NotAvailableFault, RTGException, InvalidProductFault, InvalidAmountFault, OtherPostFinanceFault, FatalInternalFault {
		
		Mockito.doReturn(rtgResponse).when(rtgDigitalService).purchase((RTGRequest) Mockito.anyObject(), (ClientAccessToken) Mockito.anyObject());
		Mockito.doReturn(token).when(authProvider).getClientAccessToken();
		
		PurchaseVoucherResponse purchaseResponse = purchaseVoucherWorkflow.execute(purchaseRequest);
		
		// Verify PurchaseVoucherRequest was properly translated into an RTGRequest
		ArgumentCaptor<RTGRequest> requestCapture = ArgumentCaptor.forClass(RTGRequest.class);
		ArgumentCaptor<ClientAccessToken> tokenCapture = ArgumentCaptor.forClass(ClientAccessToken.class);
		Mockito.verify(rtgDigitalService).purchase(requestCapture.capture(), tokenCapture.capture());
		RTGRequest rtgRequest = requestCapture.getValue();
		Assert.assertTrue("TxID translation error.", rtgRequest.getTransactionID().equals(purchaseRequest.getTxID()));
		Assert.assertTrue("ProductID translation error.", rtgRequest.getProduct().getUpc().equals(purchaseRequest.getProductID()));
		Assert.assertTrue("Amount translation error.", rtgRequest.getProduct().getAmount().equals(purchaseRequest.getAmount().toPlainString()));
		Assert.assertTrue("Currency translation error.", rtgRequest.getOrigin().getCurrencyCode().equals(purchaseRequest.getCurrency()));
		
		// Verify token was generated by AuthProvider and used
		Assert.assertTrue("Token from AuthProvider was not used.", tokenCapture.getValue() == token);
			
		// Verify PurchaseVoucherResponse was properly translated from RTGResponse
		Assert.assertTrue("PIN translation error.", purchaseResponse.getPin().equals(PIN_VALUE));
		Assert.assertTrue("Serial Number translation error.", purchaseResponse.getSerial().equals(SERIAL_VALUE));
	}

	@Test
	public void exceptionMappingTest() {
		
	}
}
